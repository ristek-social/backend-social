function requestVerification(uid) {
    var uid_json = '{"uid":"' + uid + '"}';
    var origin = window.location.origin;
    var obj = JSON.parse(uid_json);
    var sendData = JSON.stringify(obj);
    $.ajax({
        contentType: "application/json;charset=utf-8",
        type: 'POST',
        data: sendData,
        url: origin + "/auth/request-verify/",
        success: function() {
            $(".card-text").html("Check your email, we have sent you a new verification link");
         }
    });
}

