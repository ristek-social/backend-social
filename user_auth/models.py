from django.db import models
from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin
)

from rest_framework_simplejwt.tokens import RefreshToken

AUTH_PROVIDERS = {'google': 'google', 'email': 'email'}

class Membership(models.Model):
    type = models.CharField(max_length=255, db_index=True, null=False)
    price = models.IntegerField()
    number_of_topics = models.IntegerField()

    def __str__(self):
        return self.type

class Occupation(models.Model):
    code = models.CharField(max_length=255, db_index=True, null=False, primary_key=True)
    name = models.CharField(max_length=255, db_index=True, null=False)

    def __str__(self):
        return self.name

# Create your models here.
class UserManager(BaseUserManager):
    def create_user(self, fullname, username, email, occupation, password=None, membership='Free'):

        if username is None:
            raise TypeError('Users should have a username')
        if email is None:
            raise TypeError('Users should have an email')
        
        user = self.model(fullname=fullname, username=username, email=self.normalize_email(email))
        user.occupation = occupation
        user.set_password(password)
        user.save()

        membership = Membership.objects.get(type=membership)
        subscription = Subscription.objects.create(username=user, membership=membership, number_of_topics=membership.number_of_topics)
        subscription.save()
        return user
    
    def create_superuser(self, fullname, username, email, occupation, password):

        if email is None:
            raise TypeError('Users should have an email')
        if password is None:
            raise TypeError('Password should not be None')
        
        user = self.create_user(fullname=fullname, username=username, email=email, occupation=occupation, password=password, membership='Premium')
        user.is_superuser = True
        user.is_staff = True
        user.save()
        return user

class User(AbstractBaseUser, PermissionsMixin):
    fullname = models.CharField(max_length=255)
    username = models.CharField(max_length=255, unique=True, db_index=True)
    email = models.EmailField(max_length=255, unique=True, db_index=True)
    occupation = models.ForeignKey(to=Occupation, related_name='user_occupation', on_delete=models.CASCADE, null=False)
    auth_provider = models.CharField(max_length=255, blank=False, null=False, default=AUTH_PROVIDERS.get('email'))

    is_verified = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['fullname', 'username', 'occupation']

    objects = UserManager()

    def __str__(self):
        return self.username
    
    def tokens(self):
        tokens = RefreshToken.for_user(self)
        return {
            'refresh': str(tokens),
            'access': str(tokens.access_token)
        }

class Subscription(models.Model):
    username = models.ForeignKey(to=User, related_name='user', on_delete=models.CASCADE, null=False)
    membership = models.ForeignKey(to=Membership, related_name='user_membership', on_delete=models.CASCADE, null=False, default='Free')
    number_of_topics = models.IntegerField(null=False)

    def __str__(self):
        return self.username.username + ": " + self.membership.type + " " + str(self.number_of_topics)

    def memberships(self):
        return {
            'membership': str(self.membership),
            'number_of_topics': self.number_of_topics
        }
