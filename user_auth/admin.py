from django.contrib import admin
from .models import Subscription, User, Membership, Occupation

# Register your models here.
class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'fullname', 'username', 'email', 'occupation', 'auth_provider')

class MembershipAdmin(admin.ModelAdmin):
    list_display = ('id', 'type', 'price', 'number_of_topics')

class SubscriptionAdmin(admin.ModelAdmin):
    list_display = ('id', 'username', 'membership', 'number_of_topics')

class OccupationAdmin(admin.ModelAdmin):
    list_display = ('code', 'name')

admin.site.register(User, UserAdmin)
admin.site.register(Membership, MembershipAdmin)
admin.site.register(Subscription, SubscriptionAdmin)
admin.site.register(Occupation, OccupationAdmin)
