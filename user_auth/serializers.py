from django.contrib import auth
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import BadRequest, ObjectDoesNotExist, RequestAborted
from django.utils.encoding import (
    DjangoUnicodeDecodeError,
    force_str,
    smart_str,
    smart_bytes
)
from django.utils.http import (
    urlsafe_base64_decode,
    urlsafe_base64_encode
)
from django.urls import reverse

from rest_framework import serializers
from rest_framework.exceptions import AuthenticationFailed
from rest_framework_simplejwt.exceptions import TokenError
from rest_framework_simplejwt.tokens import (
    RefreshToken,
    TokenError
)

from payment.models import Payment, PaymentStatus, PaymentType
from .models import (
    Membership,
    Subscription,
    User
)
from .utils import Util

class RegisterSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=64, min_length=8, write_only=True)
    
    def validate(self, data):
        fullname = data.get('fullname', '')
        username = data.get('username', '')
        email = data.get('email', '')
        occupation = data.get('occupation', '')
        
        return data
    
    def create(self, validated_data):
        return User.objects.create_user(**validated_data)

    class Meta:
        model = User
        fields = ['fullname', 'username', 'email', 'occupation', 'password']

class EmailVerificationSerializer(serializers.ModelSerializer):
    token = serializers.CharField(max_length=600)

    class Meta:
        model = User
        fields = ['token']

class RequestNewEmailVerificationSerializer(serializers.ModelSerializer):
    uid = serializers.CharField()

    def validate(self, data):
        uid = data.get('uid', '')
        if not User.objects.filter(id=uid).exists():
            raise AuthenticationFailed("User not found")
        return data

    class Meta:
        model = User
        fields = ['uid']

class LoginSerializer(serializers.ModelSerializer):
    username = serializers.CharField(max_length=255, read_only=True)
    email = serializers.EmailField(max_length=255)
    password = serializers.CharField(max_length=64, min_length=8, write_only=True)
    membership = serializers.SerializerMethodField()
    tokens = serializers.SerializerMethodField()

    def get_tokens(self, data):
        user = User.objects.get(email=data['email'])

        return {
            'refresh': user.tokens()['refresh'],
            'access': user.tokens()['access']
        }
    
    def get_membership(self, data):
        user = User.objects.get(email=data['email'])
        subscription = Subscription.objects.get(username=user)

        return {
            'membership': subscription.memberships()['membership'],
            'number_of_topics': subscription.memberships()['number_of_topics']
        }

    def validate(self, data):
        email = data.get('email', '')
        password = data.get('password', '')
        filtered_user_by_email = User.objects.filter(email=email)

        user = auth.authenticate(email=email, password=password)

        if filtered_user_by_email.exists() and filtered_user_by_email[0].auth_provider != 'email':
            raise AuthenticationFailed(detail='Please continue your login using ' + filtered_user_by_email[0].auth_provider)
        if not user:
            raise AuthenticationFailed('Invalid credentials, try again')
        if not user.is_active:
            raise AuthenticationFailed('Account disabled')
        if not user.is_verified:
            raise AuthenticationFailed('Email is not verified')

        subscription = Subscription.objects.get(username=user)
        
        return {
            'email': user.email,
            'username': user.username,
            'membership': subscription.memberships,
            'tokens': user.tokens
        }

    class Meta:
        model = User
        fields = ['username', 'email', 'password', 'membership', 'tokens',]

class RequestPasswordEmailRequestSerializer(serializers.Serializer):
    email = serializers.EmailField(max_length=255)

    def validate(self, data):
        email = data.get('email', '')
        if User.objects.filter(email=email).exists():
            user = User.objects.get(email=email)
            uidb64 = urlsafe_base64_encode(smart_bytes(user.id))
            token = PasswordResetTokenGenerator().make_token(user)

            current_site = get_current_site(request=self.context.get('request')).domain
            relative_link = reverse('reset-password-confirm', kwargs={'uidb64': uidb64, 'token': token})
            absurl = 'http://' + current_site + relative_link
            email_body = 'Hi ' + user.username + ',\n\n' + 'Use the link below to reset your password+ \n' + absurl + '\n\nRegards,\nSocial+ Admin'
            data = {
                'domain': absurl,
                'to_email': user.email,
                'email_subject': 'Reset Your Social+ Account Password',
                'email_body': email_body
            }
            Util.send_email(data)
        return data
        
    class Meta:
        fields = ['email']

class SetPasswordSerializer(serializers.Serializer):
    password = serializers.CharField(max_length=64, min_length=8, write_only=True)
    uidb64 = serializers.CharField(min_length=1, write_only=True)
    token = serializers.CharField(min_length=1, write_only=True)

    def validate(self, data):
        try:
            password = data.get('password', '')
            uidb64 = data.get('uidb64', '')
            token = data.get('token', '')

            id = force_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(id=id)

            if not PasswordResetTokenGenerator().check_token(user, token):
                raise AuthenticationFailed('The link is invalid/has been used', 401)

            user.set_password(password)
            user.save()
            return user
        except Exception:
            raise AuthenticationFailed('The link is invalid/has been used', 401)

    class Meta:
        fields = ['password', 'uidb64', 'token']

class LogoutSerializer(serializers.Serializer):
    refresh = serializers.CharField()

    default_error_message = {
        'bad_token': ('Token is expired or invalid')
    }

    def validate(self, data):
        self.token = data['refresh']

        return data

    def save(self, **kwargs):
        try:
            RefreshToken(self.token).blacklist()
        except TokenError:
            self.fail('bad_token')

class RequestUpgradeMembershipSerializer(serializers.ModelSerializer):
    username = serializers.CharField(read_only=True)
    email = serializers.CharField(read_only=True)
    old_membership = serializers.CharField(read_only=True)
    payment_status = serializers.CharField(read_only=True)
    created_at = serializers.DateTimeField(read_only=True)
    updated_at = serializers.DateTimeField(read_only=True)
    
    def validate(self, data):
        new_membership = data.get('new_membership', '')
        payment_type = data.get('payment_type', '')
        user = self.context.get('request').user
        print("error user not found")

        membership = Membership.objects.get(type=new_membership)
        print("error get membership")
        payment = PaymentType.objects.get(name=payment_type)
        print("error get payment type")
        payment_status_pending = PaymentStatus.objects.get(name='Pending')
        print("error get payment status pending")
        payment_status_received = PaymentStatus.objects.get(name='Received')
        print("error get payment status received")
        payment_obj = Payment.objects.filter(username=user, new_membership=membership)
        print("error get payment obj")
        
        if not membership:
            print("error not membership")
            raise ObjectDoesNotExist('Membership Type does not exist')
        if not payment:
            print("error not payment type")
            raise ObjectDoesNotExist('Payment Type does not exist')

        if payment_obj.filter(payment_status=payment_status_pending).exists():
            print("error you have requested")
            raise serializers.ValidationError('You have requested to upgrade your membership')
        if payment_obj.exists() and payment_obj[0].payment_status.name.upper() == 'COMPLETE':
            print("error complete")
            raise serializers.ValidationError('Your membership has been upgraded')
        if payment_obj.filter(payment_status=payment_status_received).exists():
            print("error payment received")
            raise serializers.ValidationError('We have received your payment, please wait for the admin to change your membership')
        
        print("success")
        return data
    
    def create(self, validated_data):
        validated_data['user_id'] = self.context.get('request').user.id
        return Payment.objects.create_payment(**validated_data)

    class Meta:
        model = Payment
        fields = ['id', 'username', 'email', 'old_membership', 'new_membership', 'amount', 'payment_type', 'payment_status', 'created_at', 'updated_at']

class CompleteUpgradeMembershipSerializer(serializers.ModelSerializer):
    payment_id = serializers.IntegerField(write_only=True)
    username = serializers.CharField(read_only=True)
    email = serializers.CharField(read_only=True)
    old_membership = serializers.CharField(read_only=True)
    new_membership = serializers.CharField(read_only=True)
    payment_type = serializers.CharField(read_only=True)
    payment_status = serializers.CharField(read_only=True)
    created_at = serializers.DateTimeField(read_only=True)
    updated_at = serializers.DateTimeField(read_only=True)

    def validate(self, data):
        payment_id = data.get('payment_id', '')
        payment = Payment.objects.get(id=payment_id)

        if not payment:
            raise ObjectDoesNotExist('Payment does not exist')
        if payment.payment_status.name.upper() == 'COMPLETE':
            raise serializers.ValidationError('The payment has been completed')

        return data
    
    def create(self, validated_data):
        return Payment.objects.update_payment(**validated_data)

    class Meta:
        model = Payment
        fields = ['payment_id', 'username', 'email', 'old_membership', 'new_membership', 'amount', 'payment_type', 'payment_status', 'created_at', 'updated_at']
