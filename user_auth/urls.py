from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView
from .views import (
    RegisterView,
    VerifyEmail,
    RequestNewEmailVerification,
    LoginAPIView,
    PasswordTokenCheckerAPIView,
    RequestResetPasswordEmail,
    SetPasswordAPIView,
    LogoutAPIView,
    ProfileAPIView,
    RetrieveMembershipAPIView,
    RequestUpgradeMembershipAPIView,
    CompleteUpgradeMembershipAPIView
)

urlpatterns = [
    path('register/', RegisterView.as_view(), name='register'),
    path('verify/', VerifyEmail.as_view(), name='verify'),
    path('request-verify/', RequestNewEmailVerification.as_view(), name='request-verify'),
    path('login/', LoginAPIView.as_view(), name='login'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token-refresh'), 
    path('request-reset-password/', RequestResetPasswordEmail.as_view(), name='request-reset-password'),
    path('reset-password/<uidb64>/<token>', PasswordTokenCheckerAPIView.as_view(), name='reset-password-confirm'),
    path('reset-password-complete/', SetPasswordAPIView.as_view(), name='reset-password-complete'),
    path('logout/', LogoutAPIView.as_view(), name='logout'),
    path('profile/', ProfileAPIView.as_view(), name='profile'),
    path('retrieve-membership-type/', RetrieveMembershipAPIView.as_view(), name='retrieve-membership-type'),
    path('request-upgrade-membership/', RequestUpgradeMembershipAPIView.as_view(), name='upgrade-membership'),
    path('complete-upgrade-membership/', CompleteUpgradeMembershipAPIView.as_view(), name='complete-membership'),
]