from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags

class Util:
    @staticmethod
    def send_email(data, template_name):
        html_content = render_to_string(template_name, data['email_body'])
        text_content = strip_tags(html_content)
        email = EmailMultiAlternatives(subject=data['email_subject'], body=text_content, to=[data['to_email']])
        email.attach_alternative(html_content, "text/html")
        email.send()