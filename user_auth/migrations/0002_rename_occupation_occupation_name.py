# Generated by Django 3.2.6 on 2021-08-19 13:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user_auth', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='occupation',
            old_name='occupation',
            new_name='name',
        ),
    ]
