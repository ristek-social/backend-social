from django.conf import settings
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import (
    DjangoUnicodeDecodeError,
    smart_str,
)
from django.utils.http import urlsafe_base64_decode
from django.urls import reverse

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from rest_framework import (
    generics,
    status,
    views,
    permissions
)
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken

from .models import Membership, Subscription, User
from .renderers import UserRenderer
from .serializers import (
    RegisterSerializer,
    EmailVerificationSerializer,
    RequestNewEmailVerificationSerializer,
    LoginSerializer,
    RequestPasswordEmailRequestSerializer,
    SetPasswordSerializer,
    LogoutSerializer,
    RequestUpgradeMembershipSerializer,
    CompleteUpgradeMembershipSerializer
)
from .utils import Util
from payment.models import PaymentType, Payment

import jwt

# Create your views here.
class RegisterView(generics.GenericAPIView):
    serializer_class = RegisterSerializer
    renderer_classes = (UserRenderer,)
    template_name = "mail.html"

    def post(self, request):
        user = request.data
        serializer = self.serializer_class(data=user)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        user_data = serializer.data
        user = User.objects.get(username=user_data['username'])

        token = RefreshToken.for_user(user).access_token
        current_site = get_current_site(request).domain
        relative_link = reverse('verify')
        absurl = 'http://' + current_site + relative_link + '?token=' + str(token) + "&uid=" + str(user.id)
        data = {
            'domain': absurl,
            'to_email': user.email,
            'email_subject': 'Email Verification For Social+ Account',
            'email_body': {
                'username': user.username,
                'url': absurl
            }
        }
        Util.send_email(data, self.template_name)

        return Response({
            'success': True,
            'data': user_data
        }, status=status.HTTP_201_CREATED)

class VerifyEmail(views.APIView):
    serializer_class = EmailVerificationSerializer
    renderer_classes = (UserRenderer,TemplateHTMLRenderer)
    token_param_config = openapi.Parameter(name='token', in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description='Token used for email verification')
    template_name = "email_verification.html"

    @swagger_auto_schema(manual_parameters=[token_param_config])
    def get(self, request):
        token = request.GET.get('token')
        uid = request.GET.get('uid')
        try:
            payload = jwt.decode(token, settings.SECRET_KEY, algorithms='HS256')
            user = User.objects.get(id=payload['user_id'])

            if not user.is_verified:
                user.is_verified = True
                user.save()

            return Response({
                'success': True,
                'message': 'Email successfully activated'
            }, status=status.HTTP_200_OK, template_name=self.template_name)
        except jwt.ExpiredSignatureError:
            return Response({
                'success': False,
                'message': 'Activation Link Expired',
                'uid': uid
            }, status=status.HTTP_400_BAD_REQUEST, template_name=self.template_name)
        except jwt.exceptions.DecodeError:
            return Response({
                'success': False,
                'message': 'Invalid Token',
                'uid': uid
            }, status=status.HTTP_400_BAD_REQUEST, template_name=self.template_name)

class RequestNewEmailVerification(generics.GenericAPIView):
    serializer_class = RequestNewEmailVerificationSerializer
    renderer_classes = (UserRenderer,)
    template_name = "mail.html"

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = User.objects.get(id=serializer.data['uid'])

        token = RefreshToken.for_user(user).access_token
        current_site = get_current_site(request).domain
        relative_link = reverse('verify')
        absurl = 'http://' + current_site + relative_link + '?token=' + str(token) + "&uid=" + str(user.id)
        data = {
            'domain': absurl,
            'to_email': user.email,
            'email_subject': 'Email Verification For Social+ Account',
            'email_body': {
                'username': user.username,
                'url': absurl
            }
        }
        Util.send_email(data, self.template_name)

        return Response({
            'success': True,
            'data': serializer.data
        }, status=status.HTTP_200_OK)

class LoginAPIView(generics.GenericAPIView):
    serializer_class = LoginSerializer
    renderer_classes = (UserRenderer,)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        return Response({
            'success': True,
            'data': serializer.data
        }, status=status.HTTP_200_OK)

class RequestResetPasswordEmail(generics.GenericAPIView):
    serializer_class = RequestPasswordEmailRequestSerializer
    renderer_classes = (UserRenderer,)

    def post(self, request):
        serializer = self.serializer_class(data=request.data, context={'request':request})
        serializer.is_valid(raise_exception=True)

        return Response({
            'success': True,
            'message': 'Check your email, we have sent you a link to reset your password'
        }, status=status.HTTP_200_OK)

class PasswordTokenCheckerAPIView(generics.GenericAPIView):
    serializer_class = SetPasswordSerializer
    renderer_classes = (UserRenderer,)

    def get(self, request, uidb64, token):
        try:
            id = smart_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(id=id)

            if not PasswordResetTokenGenerator().check_token(user, token):
                return Response({
                    'success':False,
                    'message': 'Password reset link expired, please request a new one'
                }, status=status.HTTP_401_UNAUTHORIZED)
            
            return Response({
                'success': True,
                'message': 'Credentials Valid',
                'data' : {
                    'uidb64': uidb64,
                    'token': token
                }
            }, status=status.HTTP_200_OK)
        except DjangoUnicodeDecodeError:
            return Response({
                'success':False,
                'message': 'Password reset link expired, please request a new one'
            }, status=status.HTTP_401_UNAUTHORIZED)

class SetPasswordAPIView(generics.GenericAPIView):
    serializer_class = SetPasswordSerializer
    renderer_classes = (UserRenderer,)

    def patch(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        return Response({
            'success': True,
            'message': 'Reset Password Success'
        }, status=status.HTTP_200_OK)

class LogoutAPIView(generics.GenericAPIView):
    serializer_class = LogoutSerializer
    permission_classes =  (permissions.IsAuthenticated,)
    renderer_classes = (UserRenderer,)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({
            'success': True,
            'message': 'Logout Success'
        }, status=status.HTTP_204_NO_CONTENT)

class ProfileAPIView(generics.GenericAPIView):
    permission_classes =  (permissions.IsAuthenticated,)
    renderer_classes = (UserRenderer,)

    def get(self, request):
        subscription = Subscription.objects.get(username=self.request.user.id)

        try:
            payment = Payment.objects.get(username=self.request.user)

            payment_dict = {
                "id": payment.id,
                "username": payment.username.username,
                "email": payment.email,
                "old_membership": payment.old_membership.type,
                "new_membership": payment.new_membership.type,
                "amount": payment.amount,
                "payment_type": payment.payment_type.name,
                "payment_status": payment.payment_status.name,
                "created_at": "{}-{}-{}".format(payment.created_at.year, payment.created_at.month, payment.created_at.day),
                "updated_at": "{}-{}-{}".format(payment.updated_at.year, payment.updated_at.month, payment.updated_at.day)
            }
        except Payment.DoesNotExist:
            payment_dict = {}

        return Response({
            'success': True,
            'data': {
                'user': {
                    'fullname': self.request.user.fullname,
                    'username': self.request.user.username,
                    'email': self.request.user.email,
                    'occupation': self.request.user.occupation.name,
                    'auth_provider': self.request.user.auth_provider,
                    'membership': subscription.membership.type,
                    'number_of_topics': subscription.number_of_topics
                },
                "payment": payment_dict
            }
        }, status=status.HTTP_200_OK)

class RequestUpgradeMembershipAPIView(generics.GenericAPIView):
    serializer_class = RequestUpgradeMembershipSerializer
    permission_classes =  (permissions.IsAuthenticated,)
    renderer_classes = (UserRenderer,)
    template_name = "payment_mail.html"

    def post(self, request):
        print(request.data)
        serializer = self.serializer_class(context={'request':request}, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        data = serializer.data
        email_body = "Dear, {}\n\nPlease make your payment to:\n\n{}\n\nAmount:\nIDR {:,.2f}\n\nSend your payment receipt to contact@continuum.id with subject UPDATE MEMBERSHIP - USERNAME and your membership status should change when the confirmation is done.\n\nThank you!\n\nRegards,\nSocial+ Admin".format(self.request.user.username, 'nomor_rekening', data['amount'])
        email_data = {
            'to_email': self.request.user.email,
            'email_subject': 'Request Upgrade Account Membership - Social+',
            'email_body': {
                'username': self.request.user.username,
                'nomor_rekening': 'nomor_rekening',
                'amount': 'IDR {:,.2f}'.format(data['amount']),
                'confirm': False
            }
        }
        Util.send_email(email_data, self.template_name)
        
        membership = Membership.objects.get(id=data['new_membership'])
        payment_type = PaymentType.objects.get(code=data['payment_type'])
        payment_dict = {
            "id": data['id'],
            "username": data['username'],
            "email": data['email'],
            "old_membership": data['old_membership'],
            "new_membership": membership.type,
            "amount": data['amount'],
            "payment_type": payment_type.name,
            "payment_status": data['payment_status'],
            "created_at": data['created_at'],
            "updated_at": data['updated_at']
        }

        return Response({
            'success': True,
            'data': payment_dict
        }, status=status.HTTP_201_CREATED)

class CompleteUpgradeMembershipAPIView(generics.GenericAPIView):
    serializer_class = CompleteUpgradeMembershipSerializer
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser, )
    renderer_classes = (UserRenderer,)
    template_name = "payment_mail.html"

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        data = serializer.data
    
        email_data = {
            'to_email': data['email'],
            'email_subject': 'Upgrade Account Membership Confirmation - Social+',
            'email_body': {
                'username': data['username'],
                'membership': data['new_membership'],
                'confirm': True
            }
        }
        Util.send_email(email_data, self.template_name)

        return Response({
            'success': True,
            'data': data
        }, status=status.HTTP_200_OK)

class RetrieveMembershipAPIView(generics.GenericAPIView):
    renderer_classes = (UserRenderer,)

    def get(self, request):
        memberships = Membership.objects.all()
        membership_list = []

        for membership in memberships:
            dict = {}
            dict["id"] = membership.id
            dict["type"] = membership.type
            dict["price"] = membership.price
            dict["number_of_topics"] = membership.number_of_topics

            membership_list.append(dict)
        
        return Response({
            "success": True,
            "data": {
                "membership_type_list": membership_list,
            }
        }, status=status.HTTP_200_OK)
