from google.cloud import bigquery
from google.oauth2 import service_account
from .query import retrieve_query
import os

credentials = service_account.Credentials.from_service_account_file('ristek-service-account.json')
client = bigquery.Client(credentials=credentials, project=os.environ.get('PROJECT_ID'))

def retrieve_data_from_bigquery(query_type, user_id, topic_id, start_date, end_date):
    query = retrieve_query(query_type=query_type, user_id=user_id, topic_id=topic_id, start_date=start_date, end_date=end_date)
    query_job = client.query(query=query)
    results = query_job.result()
    return results.to_dataframe()