from django.urls import path
from .views import (
    InputTopicAPIView,
    DashboardAPIView,
)

urlpatterns = [
    path('input/', InputTopicAPIView.as_view(), name='input-topic'),
    path('dashboard-summary/', DashboardAPIView.as_view(), name='dashboard-summary')
]
