-- Social Media

-- sm_sentiment_summary dummy data
"""
    INSERT INTO `dashboard_dev.sm_sentiment_summary` (dw_key, period_start, account_key, subject_key, gender_key, age_key, total_sentiment_positive, total_sentiment_negative, total_sentiment_neutral, total_user, dw_created_at) VALUES
    (1, '2021-08-17', {}, {}, 1, 1, 0, 0, 1, 1, CURRENT_TIMESTAMP),
    (2, '2021-08-17', {}, {}, 1, 2, 0, 0, 1, 1, CURRENT_TIMESTAMP),
    (3, '2021-08-17', {}, {}, 2, 3, 1, 1, 2, 4, CURRENT_TIMESTAMP),
    (4, '2021-08-18', {}, {}, 1, 4, 0, 3, 0, 3, CURRENT_TIMESTAMP),
    (5, '2021-08-18', {}, {}, 2, 5, 0, 0, 1, 1, CURRENT_TIMESTAMP),
    (6, '2021-08-19', {}, {}, 1, 6, 0, 2, 0, 2, CURRENT_TIMESTAMP),
    (7, '2021-08-19', {}, {}, 2, 1, 2, 0, 0, 2, CURRENT_TIMESTAMP),
    (8, '2021-08-19', {}, {}, 2, 2, 0, 3, 0, 3, CURRENT_TIMESTAMP),
    (9, '2021-08-20', {}, {}, 1, 3, 0, 0, 4, 4, CURRENT_TIMESTAMP),
    (10, '2021-08-21', {}, {}, 2, 4, 0, 0, 1, 1, CURRENT_TIMESTAMP),
    (11, '2021-08-22', {}, {}, 1, 5, 0, 0, 1, 1, CURRENT_TIMESTAMP),
    (12, '2021-08-22', {}, {}, 1, 6, 0, 0, 2, 2, CURRENT_TIMESTAMP),
    (13, '2021-08-22', {}, {}, 2, 1, 3, 0, 0, 3, CURRENT_TIMESTAMP),
    (14, '2021-08-23', {}, {}, 2, 2, 1, 0, 0, 1, CURRENT_TIMESTAMP),
    (15, '2021-08-23', {}, {}, 2, 3, 0, 1, 0, 1, CURRENT_TIMESTAMP),
    (16, '2021-08-24', {}, {}, 1, 4, 0, 0, 2, 2, CURRENT_TIMESTAMP),
    (17, '2021-08-24', {}, {}, 1, 5, 3, 0, 0, 3, CURRENT_TIMESTAMP),
    (18, '2021-08-24', {}, {}, 2, 6, 0, 1, 0, 1, CURRENT_TIMESTAMP)
""".format(user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id)

-- Impression
"""
    SELECT SUM(total_sentiment_positive + total_sentiment_negative + total_sentiment_neutral) AS impression
    FROM dashboard_dev.sm_sentiment_summary SM
    WHERE SM.account_key = {} AND SM.subject_key = {}
""".format(user_id, topic.id),

-- Total Reach
"""
    SELECT SUM(total_user) AS total_reach
    FROM dashboard_dev.sm_sentiment_summary SM
    WHERE SM.account_key = {} AND SM.subject_key = {}
""".format(user_id, topic.id),

-- Daily Exposure
"""
    SELECT SM.period_start AS date, SUM(total_user) AS exposure
    FROM dashboard_dev.sm_sentiment_summary SM
    WHERE SM.account_key = {} AND SM.subject_key = {}
    GROUP BY SM.period_start
""".format(user_id, topic.id),

-- Sentiment
""" 
    SELECT SM.account_key, SM.subject_key, SUM(total_sentiment_positive), SUM(total_sentiment_negative), SUM(total_sentiment_neutral)
    FROM dashboard_dev.sm_sentiment_summary SM
    WHERE SM.account_key = {} AND SM.subject_key = {}
    GROUP BY SM.account_key, SM.subject_key
""".format(user_id, topic.id),

-- sm.user_profile
"""
    INSERT INTO `dashboard_dev.sm_user_profile` (dw_key, period_start, account_key, subject_key, gender_key, age_key, total_user, dw_created_at) VALUES
    (1, '2021-08-17', {}, {}, 1, 1, 1, CURRENT_TIMESTAMP),
    (2, '2021-08-17', {}, {}, 1, 2, 1, CURRENT_TIMESTAMP),
    (3, '2021-08-17', {}, {}, 2, 3, 4, CURRENT_TIMESTAMP),
    (4, '2021-08-18', {}, {}, 1, 4, 3, CURRENT_TIMESTAMP),
    (5, '2021-08-18', {}, {}, 2, 5, 1, CURRENT_TIMESTAMP),
    (6, '2021-08-19', {}, {}, 1, 6, 2, CURRENT_TIMESTAMP),
    (7, '2021-08-19', {}, {}, 2, 1, 2, CURRENT_TIMESTAMP),
    (8, '2021-08-19', {}, {}, 2, 2, 3, CURRENT_TIMESTAMP),
    (9, '2021-08-20', {}, {}, 1, 3, 4, CURRENT_TIMESTAMP),
    (10, '2021-08-21', {}, {}, 2, 4, 1, CURRENT_TIMESTAMP),
    (11, '2021-08-22', {}, {}, 1, 5, 1, CURRENT_TIMESTAMP),
    (12, '2021-08-22', {}, {}, 1, 6, 2, CURRENT_TIMESTAMP),
    (13, '2021-08-22', {}, {}, 2, 1, 3, CURRENT_TIMESTAMP),
    (14, '2021-08-23', {}, {}, 2, 2, 1, CURRENT_TIMESTAMP),
    (15, '2021-08-23', {}, {}, 2, 3, 1, CURRENT_TIMESTAMP),
    (16, '2021-08-24', {}, {}, 1, 4, 2, CURRENT_TIMESTAMP),
    (17, '2021-08-24', {}, {}, 1, 5, 3, CURRENT_TIMESTAMP),
    (18, '2021-08-24', {}, {}, 2, 6, 1, CURRENT_TIMESTAMP)
""".format(user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id)

-- Gender Profile
"""
    SELECT DG.name, SUM(total_sentiment_positive) + SUM(total_sentiment_negative) + SUM(total_sentiment_neutral)
    FROM `dashboard_dev.sm_sentiment_summary` SM
    JOIN `dashboard_dev.d_gender` DG ON SM.gender_key = DG.dw_key
    WHERE SM.account_key = {} AND SM.subject_key = {}
    GROUP BY DG.name
""".format(user_id, topic.id)

-- Age Profile
"""
    SELECT DA.name, SUM(total_sentiment_positive) + SUM(total_sentiment_negative) + SUM(total_sentiment_neutral)
    FROM `dashboard_dev.sm_sentiment_summary` SM
    JOIN `dashboard_dev.d_age` DA ON SM.age_key = DA.dw_key
    WHERE SM.account_key = {} AND SM.subject_key = {}
    GROUP BY DA.name
""".format(user_id, topic.id)

-- sm_location_summary Dummy Data
"""
    INSERT INTO `dashboard_dev.sm_location_summary` (dw_key, period_start, account_key, subject_key, city_key, province_key, total_post, dw_created_at) VALUES
    (1, '2021-08-17', {}, {}, 1, 1, 1, CURRENT_TIMESTAMP),
    (2, '2021-08-17', {}, {}, 3, 2, 5, CURRENT_TIMESTAMP),
    (3, '2021-08-18', {}, {}, 2, 2, 3, CURRENT_TIMESTAMP),
    (4, '2021-08-18', {}, {}, 1, 1, 1, CURRENT_TIMESTAMP),
    (5, '2021-08-19', {}, {}, 2, 2, 3, CURRENT_TIMESTAMP),
    (6, '2021-08-19', {}, {}, 6, 5, 2, CURRENT_TIMESTAMP),
    (7, '2021-08-19', {}, {}, 1, 1, 2, CURRENT_TIMESTAMP),
    (8, '2021-08-20', {}, {}, 1, 1, 1, CURRENT_TIMESTAMP),
    (9, '2021-08-20', {}, {}, 7, 6, 1, CURRENT_TIMESTAMP),
    (10, '2021-08-20', {}, {}, 8, 7, 1, CURRENT_TIMESTAMP),
    (11, '2021-08-20', {}, {}, 9, 8, 1, CURRENT_TIMESTAMP),
    (12, '2021-08-21', {}, {}, 1, 1, 1, CURRENT_TIMESTAMP),
    (13, '2021-08-22', {}, {}, 1, 1, 2, CURRENT_TIMESTAMP),
    (14, '2021-08-22', {}, {}, 5, 4, 1, CURRENT_TIMESTAMP),
    (15, '2021-08-22', {}, {}, 10, 9, 1, CURRENT_TIMESTAMP),
    (16, '2021-08-22', {}, {}, 11, 10, 1, CURRENT_TIMESTAMP),
    (17, '2021-08-22', {}, {}, 12, 11, 1, CURRENT_TIMESTAMP),
    (18, '2021-08-23', {}, {}, 1, 1, 2, CURRENT_TIMESTAMP),
    (19, '2021-08-24', {}, {}, 1, 1, 1, CURRENT_TIMESTAMP),
    (20, '2021-08-24', {}, {}, 4, 3, 5, CURRENT_TIMESTAMP)
""".format(user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id)

"""
    INSERT INTO `dashboard_dev.d_location` VALUES
    (6, 'Surabaya', 5, 'Jawa Timur'),
    (7, 'Semarang', 6, 'Jawa Tengah')
    (8, 'Palembang', 7, 'Sumatera Selatan')
    (9, 'Medan', 8, 'Sumatera Utara')
    (10, 'Pekanbaru', 9, 'Riau')
    (11, 'Makassar', 10, 'Sulawesi Selatan')
    (12, 'Balikpapan', 11, 'Kalimantan Timur')
"""
-- City Location
"""
    SELECT DL.name AS name, SUM(SL.total_post) OVER(PARTITION BY SL.DL.name) AS frequency, SUM(SL.total_post) as total_frequency
    FROM `dashboard_dev.sm_location_summary` SL
    JOIN `dashboard_dev.d_location` DL ON SL.city_key = DL.dw_key
    WHERE SL.account_key = {} AND SL.subject_key = {}
    LIMIT 10
""".format(user_id, topic.id)

"""
    SELECT SUM(SL.total_post) AS total_frequency
    FROM `dashboard_dev.sm_location_summary` SL
    JOIN `dashboard_dev.d_location` DL ON SL.city_key = DL.dw_key
    WHERE SL.account_key = {} AND SL.subject_key = {}
""".format(user_id, topic.id)

-- Province Location
"""
    SELECT DP.name, SUM(SL.total_post)
    FROM `dashboard_dev.sm_location_summary` SL
    JOIN `dashboard_dev.d_province` DP ON SL.province_key = DP.dw_key
    WHERE SL.account_key = {} AND SL.subject_key = {}
    GROUP BY DP.name
""".format(user_id, topic.id)

"""
    WITH d_province AS (SELECT DISTINCT(province_key), province_name
                        FROM `dashboard_dev.d_location`)
    SELECT DP.province_name, SUM(SL.total_post)
    FROM `dashboard_dev.sm_location_summary` SL
    JOIN d_province DP ON DP.province_key = SL.province_key
    WHERE SL.account_key = {} AND SL.subject_key = {}
    GROUP BY DP.province_name
""".format(user_id, topic.id)

-- sm_sentiment_topic Dummy Data
"""
    INSERT INTO `dashboard_dev.sm_sentiment_topic` (dw_key, period_start, account_key, subject_key, sentiment_status_key, topic, post_example, total_post, dw_created_at) VALUES
    (1, '2021-08-17', {}, {}, 1, '17an', 'met 17an semuanya ^^', 7, CURRENT_TIMESTAMP),
    (2, '2021-08-17', {}, {}, 2, 'indonesia', 'pengen pindah dari indonesia', 6, CURRENT_TIMESTAMP),
    (3, '2021-08-17', {}, {}, 3, 'ppkm', '17an skrg b aja krn ppkm:(', 10, CURRENT_TIMESTAMP),
    (4, '2021-08-18', {}, {}, 2, 'vibes 17an', 'bete gakerasa bgt vibes 17an', 7, CURRENT_TIMESTAMP),
    (5, '2021-08-19', {}, {}, 1, 'proud', 'im proud to be indonesian', 2, CURRENT_TIMESTAMP),
    (6, '2021-08-22', {}, {}, 3, '17 agustus', '5 hari semenjak 17 agustus', 3, CURRENT_TIMESTAMP),
    (7, '2021-08-23', {}, {}, 1, 'celebrate', 'alhamdulillah ya kmrn org2 masih celebrate 17an dengan meriah', 1, CURRENT_TIMESTAMP)
""".format(user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id),

-- N-Gram
"""
    SELECT ST.topic, ST.post_example, ST.total_post
    FROM `dashboard_dev.sm_sentiment_topic` ST
    WHERE ST.account_key = {} AND ST.subject_key = {}
""".format(user_id, topic.id)

-- sm_recap Dummy Data
"""
    INSERT INTO `dashboard_dev.sm_recap` (dw_key, period_start, account_key, subject_key, sentiment_status_key, post_created_at, content, user_name, dw_created_at) VALUES
    (1, '2021-08-17', {}, {}, 1, '2021-08-17', 'met 17an semuanya ^^', 'socialplusadmn1', CURRENT_TIMESTAMP),
    (2, '2021-08-17', {}, {}, 2, '2021-08-17', 'pengen pindah dari indonesia', 'socialplusadmn11', CURRENT_TIMESTAMP),
    (3, '2021-08-17', {}, {}, 3, '2021-08-17', '17an skrg b aja krn ppkm:(', 'socialplusadmn25', CURRENT_TIMESTAMP),
    (4, '2021-08-18', {}, {}, 2, '2021-08-18', 'bete gakerasa bgt vibes 17an', 'socialplusadmn21', CURRENT_TIMESTAMP),
    (5, '2021-08-18', {}, {}, 3, '2021-08-18', 'indonesia indonesia', 'socialplusadmn27', CURRENT_TIMESTAMP),
    (6, '2021-08-19', {}, {}, 1, '2021-08-19', 'im proud to be indonesian', 'socialplusadmn3', CURRENT_TIMESTAMP),
    (7, '2021-08-19', {}, {}, 2, '2021-08-19', '17an bener2 gakerasa deh sebel grgr ppkm', 'socialplusadmn15', CURRENT_TIMESTAMP),
    (8, '2021-08-20', {}, {}, 3, '2021-08-20', 'halo indonesia', 'socialplusadmn30', CURRENT_TIMESTAMP),
    (9, '2021-08-21', {}, {}, 3, '2021-08-21', 'walaupun telat tp met 17an semuanya hehe', 'socialplusadmn33', CURRENT_TIMESTAMP),
    (10, '2021-08-22', {}, {}, 1, '2021-08-22', 'keren bgt paskibra pas 17an kemaren', 'socialplusadmn9', CURRENT_TIMESTAMP),
    (11, '2021-08-22', {}, {}, 3, '2021-08-22', '5 hari semenjak 17 agustus', 'socialplusadmn35', CURRENT_TIMESTAMP),
    (12, '2021-08-23', {}, {}, 1, '2021-08-23', 'alhamdulillah ya kmrn org2 masih celebrate 17an dengan meriah', 'socialplusadmn6', CURRENT_TIMESTAMP),
    (13, '2021-08-23', {}, {}, 2, '2021-08-23', 'udah 17an ppkm, skrg masih jg, ppkm mulu bosen', 'socialplusadmn18', CURRENT_TIMESTAMP),
    (14, '2021-08-24', {}, {}, 1, '2021-08-24', 'bener2 kangen vibes 17an', 'socialplusadmn4', CURRENT_TIMESTAMP),
    (15, '2021-08-24', {}, {}, 2, '2021-08-24', 'masih kecewa bener2 gakerasa vibes 17an kmrn', 'socialplusadmn19', CURRENT_TIMESTAMP),
    (16, '2021-08-24', {}, {}, 3, '2021-08-24', 'dah seminggu aja dari 17an', 'socialplusadmn24', CURRENT_TIMESTAMP)
""".format(user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id),

-- Sample Tweet and Latest Tweet
"""
    SELECT SR.user_name AS account, SR.content AS tweet, DS.name AS sentiment, SR.post_created_at AS date
    FROM `dashboard_dev.sm_recap` SR
    JOIN `dashboard_dev.d_sentiment_status` DS ON SR.sentiment_status_key = DS.dw_key
    WHERE ST.account_key = {} AND ST.subject_key = {}
""".format(user_id, topic.id)

-- sm_influencing_user_weekly Dummy Data
"""
    INSERT INTO `dashboard_dev.sm_influencing_user_weekly` (dw_key, period_start, account_key, subject_key, sentiment_status_key, retweeted_user, total_retweet, user_rank, dw_created_at) VALUES
    (1, '2021-08-17', {}, {}, 1, 'socialplusadmn1', 100, 1, CURRENT_TIMESTAMP),
    (2, '2021-08-19', {}, {}, 1, 'socialplusadmn2', 95, 2, CURRENT_TIMESTAMP),
    (3, '2021-08-19', {}, {}, 1, 'socialplusadmn3', 90, 3, CURRENT_TIMESTAMP),
    (4, '2021-08-22', {}, {}, 1, 'socialplusadmn4', 87, 4, CURRENT_TIMESTAMP),
    (5, '2021-08-22', {}, {}, 1, 'socialplusadmn5', 82, 5, CURRENT_TIMESTAMP),
    (6, '2021-08-22', {}, {}, 1, 'socialplusadmn6', 79, 6, CURRENT_TIMESTAMP),
    (7, '2021-08-23', {}, {}, 1, 'socialplusadmn7', 73, 7, CURRENT_TIMESTAMP),
    (8, '2021-08-24', {}, {}, 1, 'socialplusadmn8', 61, 8, CURRENT_TIMESTAMP),
    (9, '2021-08-24', {}, {}, 1, 'socialplusadmn9', 54, 9, CURRENT_TIMESTAMP),
    (10, '2021-08-24', {}, {}, 1, 'socialplusadmn10', 52, 10, CURRENT_TIMESTAMP),
    (11, '2021-08-17', {}, {}, 2, 'socialplusadmn11', 50, 1, CURRENT_TIMESTAMP),
    (12, '2021-08-18', {}, {}, 2, 'socialplusadmn12', 49, 2, CURRENT_TIMESTAMP),
    (13, '2021-08-18', {}, {}, 2, 'socialplusadmn13', 47, 3, CURRENT_TIMESTAMP),
    (14, '2021-08-18', {}, {}, 2, 'socialplusadmn14', 43, 4, CURRENT_TIMESTAMP),
    (15, '2021-08-19', {}, {}, 2, 'socialplusadmn15', 41, 5, CURRENT_TIMESTAMP),
    (16, '2021-08-19', {}, {}, 2, 'socialplusadmn16', 38, 6, CURRENT_TIMESTAMP),
    (17, '2021-08-19', {}, {}, 2, 'socialplusadmn17', 35, 7, CURRENT_TIMESTAMP),
    (18, '2021-08-19', {}, {}, 2, 'socialplusadmn18', 34, 8, CURRENT_TIMESTAMP),
    (19, '2021-08-19', {}, {}, 2, 'socialplusadmn19', 20, 9, CURRENT_TIMESTAMP),
    (20, '2021-08-23', {}, {}, 2, 'socialplusadmn20', 19, 10, CURRENT_TIMESTAMP),
    (21, '2021-08-24', {}, {}, 2, 'socialplusadmn21', 10, 11, CURRENT_TIMESTAMP),
    (22, '2021-08-17', {}, {}, 3, 'socialplusadmn22', 75, 1, CURRENT_TIMESTAMP),
    (23, '2021-08-17', {}, {}, 3, 'socialplusadmn23', 72, 2, CURRENT_TIMESTAMP),
    (24, '2021-08-17', {}, {}, 3, 'socialplusadmn24', 64, 3, CURRENT_TIMESTAMP),
    (25, '2021-08-17', {}, {}, 3, 'socialplusadmn25', 63, 4, CURRENT_TIMESTAMP),
    (26, '2021-08-18', {}, {}, 3, 'socialplusadmn26', 58, 5, CURRENT_TIMESTAMP),
    (27, '2021-08-20', {}, {}, 3, 'socialplusadmn27', 57, 6, CURRENT_TIMESTAMP),
    (28, '2021-08-20', {}, {}, 3, 'socialplusadmn28', 53, 7, CURRENT_TIMESTAMP),
    (29, '2021-08-20', {}, {}, 3, 'socialplusadmn29', 51, 8, CURRENT_TIMESTAMP),
    (30, '2021-08-20', {}, {}, 3, 'socialplusadmn30', 49, 9, CURRENT_TIMESTAMP),
    (31, '2021-08-21', {}, {}, 3, 'socialplusadmn31', 32, 10, CURRENT_TIMESTAMP),
    (32, '2021-08-22', {}, {}, 3, 'socialplusadmn32', 30, 11, CURRENT_TIMESTAMP),
    (33, '2021-08-22', {}, {}, 3, 'socialplusadmn33', 25, 12, CURRENT_TIMESTAMP),
    (34, '2021-08-22', {}, {}, 3, 'socialplusadmn34', 24, 13, CURRENT_TIMESTAMP),
    (35, '2021-08-24', {}, {}, 3, 'socialplusadmn35', 19, 14, CURRENT_TIMESTAMP),
    (36, '2021-08-24', {}, {}, 3, 'socialplusadmn36', 10, 15, CURRENT_TIMESTAMP)
""".format(user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id),

-- KOL
"""
    SELECT DS.name, ANY_VALUE(IU.retweeted_user), ANY_VALUE(IU.total_retweet), ANY_VALUE(IU.user_rank)
    FROM `dashboard_dev.sm_influencing_user_weekly` IU
    JOIN `dashboard_dev.d_sentiment_status` DS ON IU.sentiment_status_key = DS.dw_key
    WHERE IU.account_key = {} AND IU.subject_key = {}
    GROUP BY DS.name, IU.user_rank
    ORDER BY DS.name DESC, IU.user_rank ASC
""".format(user_id, topic.id)

-- active_user_weekly Dummy Data
"""
    INSERT INTO `dashboard_dev.sm_active_user_weekly` (dw_key, period_start, account_key, subject_key, sentiment_status_key, active_user, total_tweet, user_rank, dw_created_at) VALUES
    (1, '2021-08-17', {}, {}, 1, 'socialplusadmn1', 100, 1, CURRENT_TIMESTAMP),
    (2, '2021-08-19', {}, {}, 1, 'socialplusadmn2', 95, 2, CURRENT_TIMESTAMP),
    (3, '2021-08-19', {}, {}, 1, 'socialplusadmn3', 90, 3, CURRENT_TIMESTAMP),
    (4, '2021-08-22', {}, {}, 1, 'socialplusadmn4', 87, 4, CURRENT_TIMESTAMP),
    (5, '2021-08-22', {}, {}, 1, 'socialplusadmn5', 82, 5, CURRENT_TIMESTAMP),
    (6, '2021-08-22', {}, {}, 1, 'socialplusadmn6', 79, 6, CURRENT_TIMESTAMP),
    (7, '2021-08-23', {}, {}, 1, 'socialplusadmn7', 73, 7, CURRENT_TIMESTAMP),
    (8, '2021-08-24', {}, {}, 1, 'socialplusadmn8', 61, 8, CURRENT_TIMESTAMP),
    (9, '2021-08-24', {}, {}, 1, 'socialplusadmn9', 54, 9, CURRENT_TIMESTAMP),
    (10, '2021-08-24', {}, {}, 1, 'socialplusadmn10', 52, 10, CURRENT_TIMESTAMP),
    (11, '2021-08-17', {}, {}, 2, 'socialplusadmn11', 50, 1, CURRENT_TIMESTAMP),
    (12, '2021-08-18', {}, {}, 2, 'socialplusadmn12', 49, 2, CURRENT_TIMESTAMP),
    (13, '2021-08-18', {}, {}, 2, 'socialplusadmn13', 47, 3, CURRENT_TIMESTAMP),
    (14, '2021-08-18', {}, {}, 2, 'socialplusadmn14', 43, 4, CURRENT_TIMESTAMP),
    (15, '2021-08-19', {}, {}, 2, 'socialplusadmn15', 41, 5, CURRENT_TIMESTAMP),
    (16, '2021-08-19', {}, {}, 2, 'socialplusadmn16', 38, 6, CURRENT_TIMESTAMP),
    (17, '2021-08-19', {}, {}, 2, 'socialplusadmn17', 35, 7, CURRENT_TIMESTAMP),
    (18, '2021-08-19', {}, {}, 2, 'socialplusadmn18', 34, 8, CURRENT_TIMESTAMP),
    (19, '2021-08-19', {}, {}, 2, 'socialplusadmn19', 20, 9, CURRENT_TIMESTAMP),
    (20, '2021-08-23', {}, {}, 2, 'socialplusadmn20', 19, 10, CURRENT_TIMESTAMP),
    (21, '2021-08-24', {}, {}, 2, 'socialplusadmn21', 10, 11, CURRENT_TIMESTAMP),
    (22, '2021-08-17', {}, {}, 3, 'socialplusadmn22', 75, 1, CURRENT_TIMESTAMP),
    (23, '2021-08-17', {}, {}, 3, 'socialplusadmn23', 72, 2, CURRENT_TIMESTAMP),
    (24, '2021-08-17', {}, {}, 3, 'socialplusadmn24', 64, 3, CURRENT_TIMESTAMP),
    (25, '2021-08-17', {}, {}, 3, 'socialplusadmn25', 63, 4, CURRENT_TIMESTAMP),
    (26, '2021-08-18', {}, {}, 3, 'socialplusadmn26', 58, 5, CURRENT_TIMESTAMP),
    (27, '2021-08-20', {}, {}, 3, 'socialplusadmn27', 57, 6, CURRENT_TIMESTAMP),
    (28, '2021-08-20', {}, {}, 3, 'socialplusadmn28', 53, 7, CURRENT_TIMESTAMP),
    (29, '2021-08-20', {}, {}, 3, 'socialplusadmn29', 51, 8, CURRENT_TIMESTAMP),
    (30, '2021-08-20', {}, {}, 3, 'socialplusadmn30', 49, 9, CURRENT_TIMESTAMP),
    (31, '2021-08-21', {}, {}, 3, 'socialplusadmn31', 32, 10, CURRENT_TIMESTAMP),
    (32, '2021-08-22', {}, {}, 3, 'socialplusadmn32', 30, 11, CURRENT_TIMESTAMP),
    (33, '2021-08-22', {}, {}, 3, 'socialplusadmn33', 25, 12, CURRENT_TIMESTAMP),
    (34, '2021-08-22', {}, {}, 3, 'socialplusadmn34', 24, 13, CURRENT_TIMESTAMP),
    (35, '2021-08-24', {}, {}, 3, 'socialplusadmn35', 19, 14, CURRENT_TIMESTAMP),
    (36, '2021-08-24', {}, {}, 3, 'socialplusadmn36', 10, 15, CURRENT_TIMESTAMP)
""".format(user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id, user_id, topic.id),

-- Active User
"""
    SELECT DS.name, ANY_VALUE(AU.active_user), ANY_VALUE(AU.total_tweet), ANY_VALUE(AU.user_rank)
    FROM `dashboard_dev.sm_active_user_weekly` AU
    JOIN `dashboard_dev.d_sentiment_status` DS ON AU.sentiment_status_key = DS.dw_key
    WHERE AU.account_key = {} AND AU.subject_key = {}
    GROUP BY DS.name, AU.user_rank
    ORDER BY DS.name DESC, AU.user_rank ASC
""".format(user_id, topic.id)


-- Online Media

-- om_sentiment_summary Dummy Data
"""INSERT INTO `dashboard_dev.d_source` VALUES
(1, Instagram, Social Media, Instagram, CURRENT_TIMESTAMP),
(2, Twitter, Social Media, Twitter, CURRENT_TIMESTAMP),
(3, Facebook, Social Media, Facebook, CURRENT_TIMESTAMP),
(4, Kumparan, Online Media, Kumparan, CURRENT_TIMESTAMP),
(5, Detik, Online Media, Detik, CURRENT_TIMESTAMP),
(6, Kompas, Online Media, Kompas, CURRENT_TIMESTAMP),
(7, Tribun, Online Media, Tribun, CURRENT_TIMESTAMP),
(8, Tempo, Online Media, Tempo, CURRENT_TIMESTAMP)""",

"""INSERT INTO `dashboard_dev.om_sentiment_summary` (dw_key, period_start, account_key, subject_key, source_key, total_sentiment_positive, total_sentiment_negative, total_sentiment_neutral, dw_created_at) VALUES
(1, '2021-08-21', {}, {}, 1, 1, 0, 0, CURRENT_TIMESTAMP),
(2, '2021-08-21', {}, {}, 2, 1, 0, 0, CURRENT_TIMESTAMP),
(3, '2021-08-21', {}, {}, 3, 1, 1, 0, CURRENT_TIMESTAMP),
(4, '2021-08-22', {}, {}, 4, 0, 2, 0, CURRENT_TIMESTAMP),
(5, '2021-08-22', {}, {}, 5, 2, 0, 1, CURRENT_TIMESTAMP),
(6, '2021-08-23', {}, {}, 6, 2, 0, 0, CURRENT_TIMESTAMP),
(7, '2021-08-23', {}, {}, 7, 2, 0, 0, CURRENT_TIMESTAMP),
(8, '2021-08-23', {}, {}, 8, 1, 1, 0, CURRENT_TIMESTAMP),
(9, '2021-08-24', {}, {}, 1, 1, 0, 4, CURRENT_TIMESTAMP),
(10, '2021-08-25', {}, {}, 2, 1, 0, 1, CURRENT_TIMESTAMP),
(11, '2021-08-26', {}, {}, 3, 1, 0, 1, CURRENT_TIMESTAMP),
(12, '2021-08-26', {}, {}, 4, 0, 0, 3, CURRENT_TIMESTAMP),
(13, '2021-08-26', {}, {}, 5, 2, 0, 0, CURRENT_TIMESTAMP),
(14, '2021-08-27', {}, {}, 6, 1, 0, 0, CURRENT_TIMESTAMP),
(15, '2021-08-27', {}, {}, 7, 0, 1, 0, CURRENT_TIMESTAMP),
(16, '2021-08-28', {}, {}, 8, 0, 0, 1, CURRENT_TIMESTAMP),
(17, '2021-08-28', {}, {}, 1, 2, 0, 1, CURRENT_TIMESTAMP),
(18, '2021-08-28', {}, {}, 2, 0, 1, 0, CURRENT_TIMESTAMP)""".format(user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id),

-- Sentiment 
""" 
    SELECT SUM(total_sentiment_positive) AS positive, SUM(total_sentiment_negative) AS negative, SUM(total_sentiment_neutral) AS neutral
    FROM `dashboard_dev.om_sentiment_summary` OM
    WHERE OM.account_key = {} AND OM.subject_key = {} AND OM.period_start BETWEEN '{}' AND '{}'
    GROUP BY OM.account_key, OM.subject_key
""".format(user_id, topic_id, start_date, end_date)

-- News Trend daily_news_trend
"""
    SELECT OM.period_start AS date, SUM(total_sentiment_positive + total_sentiment_negative + total_sentiment_neutral) AS daily_trend
    FROM `dashboard_dev.om_sentiment_summary` OM
    WHERE OM.account_key = {} AND OM.subject_key = {} AND OM.period_start BETWEEN '{}' AND '{}'
    GROUP BY OM.period_start
""".format(user_id, topic_id, start_date, end_date)

-- om_sentiment_topic Dummy Data
"""INSERT INTO `dashboard_dev.om_sentiment_topic` (dw_key, period_start, account_key, subject_key, sentiment_status_key, source_key, topic, news_example, total_news, dw_created_at) VALUES
(1, '2021-08-24', {}, {}, 1, 5, 'meriah', 'WOW! Orang-orang merayakan dengan meriah', 2, CURRENT_TIMESTAMP),
(2, '2021-08-24', {}, {}, 2, 8, 'meriah', 'Sangat disayangkan 17an kali ini tidak semeriah yang diharapkan', 3, CURRENT_TIMESTAMP),
(3, '2021-08-22', {}, {}, 1, 7, '17an', 'Selamat 17an! Cek Fakta Beriku!', 1, CURRENT_TIMESTAMP),
(4, '2021-08-25', {}, {}, 3, 1, '17an', '17an sekarang biasa ajaya', 2, CURRENT_TIMESTAMP),
(5, '2021-08-27', {}, {}, 1, 7, 'Kemerdekaan', 'Selamat Hari Kemerdekaan! Cek 8 Fakta Ini, Nomor 9 Mencengangkan', 2, CURRENT_TIMESTAMP),
(6, '2021-08-28', {}, {}, 1, 4, 'Indonesia', 'Selamat Hari Jadi Indonesia!', 5, CURRENT_TIMESTAMP),
(7, '2021-08-23', {}, {}, 2, 6, 'Indonesia', 'Sedih! Banyak Warga yang Ingin Pindah Dari Indonesia', 3, CURRENT_TIMESTAMP),
(8, '2021-08-23', {}, {}, 1, 5, 'Ulang Tahun', 'Selamat Ulang Tahun NKRI!', 3, CURRENT_TIMESTAMP),
(9, '2021-08-21', {}, {}, 1, 3, 'PPKM', '17an ppkm? Gak masalah! Lakukan 100 Aktivitas Berikut Agar Tidak Bosan!', 4, CURRENT_TIMESTAMP),
(10, '2021-08-22', {}, {}, 2, 2, 'PPKM', 'sedih 17an pas ppkm', 4, CURRENT_TIMESTAMP),
(11, '2021-08-26', {}, {}, 3, 1, 'PPKM', 'PPKM TEROOOS', 7, CURRENT_TIMESTAMP)""".format(user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id)

-- N Gram n_gram
"""
    SELECT OT.topic AS topic, ANY_VALUE(OT.news_example) AS sample_news, SUM(OT.total_news) AS frequency
    FROM `dashboard_dev.om_sentiment_topic` OT
    WHERE OT.account_key = {} AND OT.subject_key = {} AND OT.period_start BETWEEN '{}' AND '{}'
    GROUP BY OT.topic
""".format(user_id, topic_id, start_date, end_date)

-- om_recap
"""INSERT INTO `dashboard_dev.om_recap` (dw_key, period_start, account_key, subject_key, sentiment_status_key, source_key, title, content, link, news_published_at, dw_created_at) VALUES
(1, '2021-08-24', {}, {}, 1, 5, 'WOW! Orang-orang merayakan dengan meriah', 'WOW! Orang-orang merayakan dengan meriah', 'https://www.detik.com/', '2021-08-17', CURRENT_TIMESTAMP),
(2, '2021-08-24', {}, {}, 2, 8, 'Sangat disayangkan 17an kali ini tidak semeriah yang diharapkan', 'Sangat disayangkan 17an kali ini tidak semeriah yang diharapkan', 'https://en.tempo.co/', '2021-08-18', CURRENT_TIMESTAMP),
(3, '2021-08-22', {}, {}, 1, 7, 'Selamat 17an! Cek Fakta Beriku!', 'Selamat 17an! Cek Fakta Beriku!', 'https://www.tribunnews.com/', '2021-08-17', CURRENT_TIMESTAMP),
(4, '2021-08-25', {}, {}, 3, 1, '17an sekarang biasa ajaya', '17an sekarang biasa ajaya', 'https://www.instagram.com/', '2021-08-19', CURRENT_TIMESTAMP),
(5, '2021-08-27', {}, {}, 1, 7, 'Selamat Hari Kemerdekaan! Cek 8 Fakta Ini, Nomor 9 Mencengangkan', 'Selamat Hari Kemerdekaan! Cek 8 Fakta Ini, Nomor 9 Mencengangkan', 'https://www.tribunnews.com/', '2021-08-17', CURRENT_TIMESTAMP),
(6, '2021-08-28', {}, {}, 1, 4, 'Selamat Hari Jadi Indonesia!', 'Selamat Hari Jadi Indonesia!', 'https://kumparan.com/', '2021-08-17', CURRENT_TIMESTAMP),
(7, '2021-08-23', {}, {}, 2, 6, 'Sedih! Banyak Warga yang Ingin Pindah Dari Indonesia', 'Sedih! Banyak Warga yang Ingin Pindah Dari Indonesia', 'https://www.kompas.com/', '2021-08-20', CURRENT_TIMESTAMP),
(8, '2021-08-23', {}, {}, 1, 5, 'Selamat Ulang Tahun NKRI!', 'Selamat Ulang Tahun NKRI!', 'https://www.detik.com/', '2021-08-17', CURRENT_TIMESTAMP),
(9, '2021-08-21', {}, {}, 1, 3, '17an ppkm? Gak masalah! Lakukan 100 Aktivitas Berikut Agar Tidak Bosan!', '17an ppkm? Gak masalah! Lakukan 100 Aktivitas Berikut Agar Tidak Bosan!', 'https://www.facebook.com/', '2021-08-17', CURRENT_TIMESTAMP),
(10, '2021-08-22', {}, {}, 2, 2, 'sedih 17an pas ppkm', 'sedih 17an pas ppkm', 'https://twitter.com/home', '2021-08-20', CURRENT_TIMESTAMP),
(11, '2021-08-26', {}, {}, 3, 1, 'PPKM TEROOOS', 'PPKM TEROOOS', 'https://www.instagram.com/', '2021-08-23', CURRENT_TIMESTAMP),
(12, '2021-08-23', {}, {}, 1, 5, 'WOW! Ulang Tahun Indonesia! WOW!', 'WOW! Ulang Tahun Indonesia! WOW!', 'https://www.detik.com/', '2021-08-21', CURRENT_TIMESTAMP),
(13, '2021-08-24', {}, {}, 2, 2, 'meriah ga meriah ttp biasa aja', 'meriah ga meriah ttp biasa aja', 'https://twitter.com/home', '2021-08-22', CURRENT_TIMESTAMP),
(14, '2021-08-26', {}, {}, 3, 1, '17an? ppkm? sudah biasa~', '17an? ppkm? sudah biasa~', 'https://www.instagram.com/', '2021-08-17', CURRENT_TIMESTAMP),
(15, '2021-08-23', {}, {}, 2, 6, 'sedih deh temen2 gue pindah dr indonesia', 'sedih deh temen2 gue pindah dr indonesia', 'https://www.kompas.com/', '2021-08-23', CURRENT_TIMESTAMP),
(16, '2021-08-24', {}, {}, 2, 8, 'Bosen bete sedih seneng gatau deh campur aduk pas 17an kmrn', 'Bosen bete sedih seneng gatau deh campur aduk pas 17an kmrn', 'https://en.tempo.co/', '2021-08-24', CURRENT_TIMESTAMP)
""".format(user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id, user_id, topic_id)

-- Total News total_new
"""
    SELECT COUNT(*) AS total_news
    FROM `dashboard_dev.om_recap` OE
    WHERE OE.account_key = {} AND OE.subject_key = {} AND OE.period_start BETWEEN '{}' AND '{}'
""".format(user_id, topic_id, start_date, end_date)

-- Latest News latest_news
"""
    SELECT OE.news_published_at AS date, DS.name AS sentiment, DD.name AS media, OE.title AS title, OE.link AS link
    FROM `dashboard_dev.om_recap` OE
    JOIN `dashboard_dev.d_sentiment_status` DS ON OE.sentiment_status_key = DS.dw_key
    JOIN `dashboard_dev.d_source` DD ON OE.source_key = DD.dw_key
    WHERE OE.account_key = {} AND OE.subject_key = {} AND OE.period_start BETWEEN '{}' AND '{}'
""".format(user_id, topic_id, start_date, end_date)

-- Media Coverage media_coverage
"""
    SELECT DD.name AS media, COUNT(*) AS frequency
    FROM `dashboard_dev.om_recap` OE
    JOIN `dashboard_dev.d_source` DD ON OE.source_key = DD.dw_key
    WHERE OE.account_key = {} AND OE.subject_key = {} AND OE.period_start BETWEEN '{}' AND '{}'
""".format(user_id, topic_id, start_date, end_date)