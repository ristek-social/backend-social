from rest_framework import (
    generics,
    status,
    permissions
)

from rest_framework.response import Response

from user_auth.models import Subscription
from .models import Topic
from .serializers import (
    TopicSerializer,
    DashboardSerializer
)

from .utils import retrieve_data_from_bigquery

import json, os, requests, concurrent.futures, re

provinces = {'Aceh': 'AC', 'Kalimantan Timur': 'KI', 'Jawa Barat': 'JR',
             'Jawa Tengah': 'JT', 'Bengkulu': 'BE', 'Banten': 'BT',
             'Jakarta Raya': 'JK', 'Kalimantan Barat': 'KB', 'Lampung': 'LA',
             'Sumatera Selatan': 'SL', 'Bangka-Belitung': 'BB', 'Bali': 'BA',
             'Jawa Timur': 'JI', 'Kalimantan Selatan': 'KS', 'Nusa Tenggara Timur': 'NT',
             'Sulawesi Selatan': 'SE', 'Sulawesi Barat': 'SR', 'Kepulauan Riau': 'KR',
             'Gorontalo': 'GO', 'Jambi': 'JA', 'Kalimantan Tengah': 'KT',
             'Irian Jaya Barat': 'IB', 'Sumatera Utara': 'SU', 'Riau': 'RI',
             'Sulawesi Utara': 'SW', 'Maluku Utara': 'MU', 'Sumatera Barat': 'SB',
             'Yogyakarta': 'YO', 'Maluku': 'MA', 'Nusa Tenggara Barat': 'NB',
             'Sulawesi Tenggara': 'SG', 'Sulawesi Tengah': 'ST', 'Papua': 'PA'
            }

# Create your views here.
class InputTopicAPIView(generics.ListCreateAPIView):
    serializer_class = TopicSerializer
    permission_classes =  (permissions.IsAuthenticated,)
    queryset = Topic.objects.all()

    def create(self, request):
        subscription = Subscription.objects.get(username=self.request.user.id)
        if subscription.number_of_topics == 0:
            return Response({
                'success': False,
                'error': 'Number of topics limit exceeded'
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        subscription.number_of_topics -= 1
        subscription.save()

        topic = Topic.objects.get(id=serializer.data['id'], topic=serializer.data['topic'], owner=self.request.user)
        url = os.environ.get('CONTINUUM_DATA_SERVICE')
        headers = {'content-type': 'application/json'}
        payload = {
            "user_id": self.request.user.id,
            "username": self.request.user.username,
            "membership": subscription.membership.type,
            "date": "{}-{}-{}".format(topic.created_at.year, topic.created_at.month, topic.created_at.day),
            "topic_id": topic.id,
            "topic": topic.topic,
            "keyword": serializer.data['keyword']
        }
        response = requests.post(url=url, data=json.dumps(payload), headers=headers)

        return Response({
            'success': True,
            'data': response.json()['data']
        }, status=status.HTTP_201_CREATED)
        
    def perform_create(self, serializer):
        serializer.is_valid(raise_exception=True)
        return serializer.save(owner=self.request.user)
        
    def get(self, request):
        subscription = Subscription.objects.get(username=self.request.user.id)
        topics = self.queryset.filter(owner=self.request.user)
        topic_list = []

        for topic in topics:
            dict = {}
            dict["id"] = topic.id
            dict["topic"] = topic.topic
            dict["keyword"] = re.split(", |,", topic.keyword)
            dict["created_at"] = topic.created_at

            topic_list.append(dict)
        
        return Response({
            "success": True,
            "data": {
                "number_of_topics": subscription.number_of_topics,
                "topic_list": topic_list,
            }
        }, status=status.HTTP_200_OK)

class DashboardAPIView(generics.GenericAPIView):
    serializer_class = DashboardSerializer
    permission_classes =  (permissions.IsAuthenticated,)

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            topic = Topic.objects.get(id=serializer.data['topic_id'], owner=self.request.user)
        except Topic.DoesNotExist:
            return Response({
                "success": False,
                "error": "Topic not found"
            }, status=status.HTTP_404_NOT_FOUND)

        if serializer.data['dashboard_type'].upper() == 'SOCIAL MEDIA':
            topic_info_dict = self.get_summary_sm(topic_id=topic.id, start_date=serializer.data['start_date'], end_date=serializer.data['end_date'])

            return Response({
                "user": self.request.user.username,
                "topic_id": topic.id,
                "title": topic.topic,
                "keyword": re.split(", |,", topic.keyword),
                "start_date": serializer.data['start_date'],
                "end_date": serializer.data["end_date"],
                "impression": topic_info_dict["impression"],
                "total_reach": topic_info_dict["total_reach"],
                "sentiment": topic_info_dict["sentiment"],
                "daily_exposure": topic_info_dict["daily_exposure"],
                "latest_tweets": topic_info_dict["latest_tweet"],
                "age_profile": topic_info_dict["age_profile"],
                "gender_profile": topic_info_dict["gender_profile"],
                "location": {
                    "provinces": topic_info_dict["province_location"],
                    "top_provinces": topic_info_dict["top_provinces"],
                    "top_cities": topic_info_dict["top_cities"][:11]
                },
                "key_opinion_leader": topic_info_dict["kol"],
                "most_active_users": topic_info_dict["active_user"],
                "n_gram": topic_info_dict["n_gram"]
            }, status=status.HTTP_200_OK)

        elif serializer.data['dashboard_type'].upper() == 'ONLINE MEDIA':
            topic_info_dict = self.get_summary_om(topic_id=topic.id, start_date=serializer.data['start_date'], end_date=serializer.data['end_date'])
            
            return Response({
                "user": self.request.user.username,
                "topic_id": topic.id,
                "title": topic.topic,
                "keyword": re.split(", |,", topic.keyword),
                "start_date": serializer.data['start_date'],
                "end_date": serializer.data["end_date"],
                "total_news": topic_info_dict["total_news"],
                "sentiment": topic_info_dict["sentiment"],
                "news_trend": topic_info_dict["news_trend"],
                "latest_news": topic_info_dict["latest_news"],
                "n_gram": topic_info_dict["n_gram"],
                "media_coverage": topic_info_dict["media_coverage"],
            }, status=status.HTTP_200_OK)
        
        else:
            return Response({
                "success": False,
                "message": "Dashboard Type Not Found"
            }, status=status.HTTP_404_NOT_FOUND)
    
    def get_summary_sm(self, topic_id, start_date, end_date):
        topic_info_dict = {}

        with concurrent.futures.ThreadPoolExecutor() as executor:
            impression = executor.submit(self.transform_impression_total_reach, "impression", topic_id, start_date, end_date)
            total_reach = executor.submit(self.transform_impression_total_reach, "total_reach", topic_id, start_date, end_date)
            sentiment = executor.submit(self.transform_sentiment, "sentiment_sm", topic_id, start_date, end_date)
            daily_exposure = executor.submit(self.transform_exposure, topic_id, start_date, end_date)
            age_profile = executor.submit(self.transform_age, topic_id, start_date, end_date)
            gender_profile = executor.submit(self.transform_gender, topic_id, start_date, end_date)
            latest_tweet = executor.submit(self.transform_tweet, topic_id, start_date, end_date)
            n_gram = executor.submit(self.transform_n_gram_sm, topic_id, start_date, end_date)
            province_location = executor.submit(self.transform_location, "province_location", topic_id, start_date, end_date)
            top_cities = executor.submit(self.transform_location, "top_cities", topic_id, start_date, end_date)
            kol = executor.submit(self.transform_user, "kol", topic_id, start_date, end_date)
            active_user = executor.submit(self.transform_user, "active_user", topic_id, start_date, end_date)

            topic_info_dict["impression"] = impression.result()
            topic_info_dict["total_reach"] = total_reach.result()
            topic_info_dict["sentiment"] = sentiment.result()
            topic_info_dict["daily_exposure"] = daily_exposure.result()
            topic_info_dict["latest_tweet"] = latest_tweet.result()
            topic_info_dict["age_profile"] = age_profile.result()
            topic_info_dict["gender_profile"] = gender_profile.result()
            topic_info_dict["n_gram"] = n_gram.result()
            topic_info_dict["province_location"] = province_location.result()
            topic_info_dict["top_provinces"] = topic_info_dict["province_location"][:11]
            topic_info_dict["province_location"].pop(10) if len(topic_info_dict["province_location"]) > 10 else None
            topic_info_dict["top_cities"] = top_cities.result()
            topic_info_dict["kol"] = kol.result()
            topic_info_dict["active_user"] = active_user.result()

        return topic_info_dict

    def get_summary_om(self, topic_id, start_date, end_date):
        topic_info_dict = {}

        with concurrent.futures.ThreadPoolExecutor() as executor:
            total_news = executor.submit(self.transform_impression_total_reach, "total_news", topic_id, start_date, end_date)
            sentiment = executor.submit(self.transform_sentiment, "sentiment_om", topic_id, start_date, end_date)
            news_trend = executor.submit(self.transform_news_trend, topic_id, start_date, end_date)
            latest_news = executor.submit(self.transform_news, topic_id, start_date, end_date)
            n_gram = executor.submit(self.transform_n_gram_om, topic_id, start_date, end_date)
            media_coverage = executor.submit(self.transform_media_coverage, topic_id, start_date, end_date)

            topic_info_dict["total_news"] = total_news.result()
            topic_info_dict["sentiment"] = sentiment.result()
            topic_info_dict["news_trend"] = news_trend.result()
            topic_info_dict["latest_news"] = latest_news.result()
            topic_info_dict["n_gram"] = n_gram.result()
            topic_info_dict["media_coverage"] = media_coverage.result()

        return topic_info_dict

    def transform_impression_total_reach(self, query_type, topic_id, start_date, end_date):
        df = retrieve_data_from_bigquery(query_type=query_type, user_id=self.request.user.id, topic_id=topic_id, start_date=start_date, end_date=end_date)
        df = df.fillna(0)
        return df[query_type][0]

    def transform_sentiment(self, query_type, topic_id, start_date, end_date):
        df = retrieve_data_from_bigquery(query_type=query_type, user_id=self.request.user.id, topic_id=topic_id, start_date=start_date, end_date=end_date)

        dict = {
            "positive": 0,
            "positive_percentage": 0.0,
            "negative": 0,
            "negative_percentage": 0.0,
            "neutral": 0,
            "neutral_percentage": 0.0
        }

        if df.empty:
            return dict

        df_new = df.transpose()
        df_new.columns = ['frequency']
        for index, row in df_new.iterrows():
            dict[index] = row[0]
            dict[index + "_percentage"] = "{:.2f}".format((row[0]/df_new['frequency'].sum())*100)
        return dict
    
    def transform_exposure(self, topic_id, start_date, end_date):
        df = retrieve_data_from_bigquery(query_type="daily_exposure", user_id=self.request.user.id, topic_id=topic_id, start_date=start_date, end_date=end_date)

        array_of_dates = []
        for _, row in df.iterrows():
            dict = {}
            dict["date"] = row["date"]
            dict["exposure"] = row["exposure"]
            array_of_dates.append(dict)
        return array_of_dates
    
    def transform_tweet(self, topic_id, start_date, end_date):
        df = retrieve_data_from_bigquery(query_type="latest_tweet", user_id=self.request.user.id, topic_id=topic_id, start_date=start_date, end_date=end_date)

        array_of_tweets = []
        for _, row in df.iterrows():
            dict = {}
            dict["account"] = row["account"]
            dict["tweet"] = row["tweet"]
            dict["sentiment"] = row["sentiment"]
            dict["date"] = row["date"]
            array_of_tweets.append(dict)
        return array_of_tweets

    def transform_age(self, topic_id, start_date, end_date):
        df = retrieve_data_from_bigquery(query_type="age_profile", user_id=self.request.user.id, topic_id=topic_id, start_date=start_date, end_date=end_date)

        array_of_age_group = []
        for _, row in df.iterrows():
            dict = {}
            dict["range"] = row["age_range"]
            dict["frequency"] = row["frequency"]
            array_of_age_group.append(dict)
        return array_of_age_group

    def transform_gender(self, topic_id, start_date, end_date):
        df = retrieve_data_from_bigquery(query_type="gender_profile", user_id=self.request.user.id, topic_id=topic_id, start_date=start_date, end_date=end_date)

        dict = {}
        for _, row in df.iterrows():
            dict[row["gender"]] = row["frequency"]
        return dict
    
    def transform_location(self, query_type, topic_id, start_date, end_date):
        df = retrieve_data_from_bigquery(query_type=query_type, user_id=self.request.user.id, topic_id=topic_id, start_date=start_date, end_date=end_date)

        array_of_locations = []
        
        if df.empty:
            return array_of_locations

        total_of_others = df["total_frequency"][0]
        top_provinces = 0
        for _, row in df.iterrows():
            dict = {}
            dict["name"] = row["name"] if query_type == 'top_cities' else provinces[row["name"]]
            dict["frequency"] = row["frequency"]
            if top_provinces < 10:
                total_of_others -= row["frequency"]
                top_provinces += 1
            elif top_provinces == 10:
                others_dict = {
                    "name": "Others",
                    "frequency": total_of_others
                }
                array_of_locations.append(others_dict)
                top_provinces += 1
            array_of_locations.append(dict)
        return array_of_locations
    
    def transform_user(self, query_type, topic_id, start_date, end_date):
        df = retrieve_data_from_bigquery(query_type=query_type, user_id=self.request.user.id, topic_id=topic_id, start_date=start_date, end_date=end_date)

        positive_array = []
        negative_array = []
        for _, row in df.iterrows():
            dict = {}
            dict["account"] = row["account"]
            dict["frequency"] = row["frequency"]
            
            if row["sentiment_type"].upper() == "POSITIVE":
                positive_array.append(dict)
            elif row["sentiment_type"].upper() == "NEGATIVE":
                negative_array.append(dict)
        return {
            "positive": positive_array,
            "negative": negative_array
        }
        
    def transform_n_gram_sm(self, topic_id, start_date, end_date):
        df = retrieve_data_from_bigquery(query_type="n_gram_sm", user_id=self.request.user.id, topic_id=topic_id, start_date=start_date, end_date=end_date)

        array_of_n_grams = []
        for _, row in df.iterrows():
            dict = {}
            dict["keyword"] = row["keyword"]
            dict["frequency"] = row["frequency"]
            dict["sample_tweet"] = row["sample_tweet"]
            array_of_n_grams.append(dict)
        return array_of_n_grams

    def transform_news_trend(self, topic_id, start_date, end_date):
        df = retrieve_data_from_bigquery(query_type="news_trend", user_id=self.request.user.id, topic_id=topic_id, start_date=start_date, end_date=end_date)

        array_of_dates = []
        for _, row in df.iterrows():
            dict = {}
            dict["date"] = row["date"]
            dict["daily_trend"] = row["daily_trend"]
            array_of_dates.append(dict)
        return array_of_dates

    def transform_news(self, topic_id, start_date, end_date):
        df = retrieve_data_from_bigquery(query_type="latest_news", user_id=self.request.user.id, topic_id=topic_id, start_date=start_date, end_date=end_date)

        array_of_news = []
        for _, row in df.iterrows():
            dict = {}
            dict["date"] = row["date"]
            dict["sentiment"] = row["sentiment"]
            dict["media"] = row["media"]
            dict["title"] = row["title"]
            dict["link"] = row["link"]
            array_of_news.append(dict)
        return array_of_news

    def transform_n_gram_om(self, topic_id, start_date, end_date):
        df = retrieve_data_from_bigquery(query_type="n_gram_om", user_id=self.request.user.id, topic_id=topic_id, start_date=start_date, end_date=end_date)

        array_of_n_grams = []
        for _, row in df.iterrows():
            dict = {}
            dict["keyword"] = row["keyword"]
            dict["frequency"] = row["frequency"]
            dict["sample_news"] = row["sample_news"]
            array_of_n_grams.append(dict)
        return array_of_n_grams

    def transform_media_coverage(self, topic_id, start_date, end_date):
        df = retrieve_data_from_bigquery(query_type="media_coverage", user_id=self.request.user.id, topic_id=topic_id, start_date=start_date, end_date=end_date)

        array_of_media = []
        for _, row in df.iterrows():
            dict = {}
            dict["media"] = row["media"]
            dict["frequency"] = row["frequency"]
            array_of_media.append(dict)
        return array_of_media