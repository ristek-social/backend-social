from rest_framework import serializers
from .models import Topic
import re

class TopicSerializer(serializers.ModelSerializer):
    created_at = serializers.DateTimeField(read_only=True)

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret['keyword'] = re.split(", |,", ret['keyword'])
        return ret

    class Meta:
        model = Topic
        fields = ['id', 'topic', 'keyword', 'created_at']

class DashboardSerializer(serializers.Serializer):
    topic_id = serializers.IntegerField()
    dashboard_type = serializers.CharField()
    start_date = serializers.DateField()
    end_date = serializers.DateField()

    class Meta:
        fields = ['topic_id', 'start_date', 'end_date']