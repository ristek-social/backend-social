from django.contrib import admin
from .models import Topic

# Register your models here.
class TopicAdmin(admin.ModelAdmin):
    list_display = ('id', 'owner', 'topic', 'keyword', 'created_at')

admin.site.register(Topic, TopicAdmin)
