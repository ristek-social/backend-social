from django.db import models
from user_auth.models import User

# Create your models here.
class Topic(models.Model):
    topic = models.CharField(max_length=255, db_index=True, null=False)
    keyword = models.CharField(max_length=2048, null=False)
    owner = models.ForeignKey(to=User, related_name='owner', on_delete=models.CASCADE, null=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.id) + " (" + self.owner.username + "): " + self.topic
