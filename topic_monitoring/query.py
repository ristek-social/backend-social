query_dict = {
    "impression": """SELECT SUM(total_sentiment_positive + total_sentiment_negative + total_sentiment_neutral) AS impression
                     FROM `dashboard_dev.sm_sentiment_summary` SM
                     WHERE SM.account_key = {} AND SM.subject_key = {} AND SM.period_start BETWEEN '{}' AND '{}'""",
    "total_reach": """SELECT SUM(total_user) AS total_reach
                      FROM `dashboard_dev.sm_sentiment_summary` SM
                      WHERE SM.account_key = {} AND SM.subject_key = {} AND SM.period_start BETWEEN '{}' AND '{}'""",
    "sentiment_sm": """SELECT SUM(total_sentiment_positive) AS positive, SUM(total_sentiment_negative) AS negative, SUM(total_sentiment_neutral) AS neutral
                            FROM `dashboard_dev.sm_sentiment_summary` SM
                            WHERE SM.account_key = {} AND SM.subject_key = {} AND SM.period_start BETWEEN '{}' AND '{}'
                            GROUP BY SM.account_key, SM.subject_key""",
    "daily_exposure": """SELECT SM.period_start AS date, SUM(total_user) AS exposure
                         FROM `dashboard_dev.sm_sentiment_summary` SM
                         WHERE SM.account_key = {} AND SM.subject_key = {} AND SM.period_start BETWEEN '{}' AND '{}'
                         GROUP BY SM.period_start
                         ORDER BY SM.period_start""",
    "age_profile": """SELECT DA.dw_key, DA.name AS age_range, SUM(UP.total_user) AS frequency
                      FROM `dashboard_dev.sm_user_profile` UP
                      JOIN `dashboard_dev.d_age` DA ON UP.age_key = DA.dw_key
                      WHERE UP.account_key = {} AND UP.subject_key = {} AND UP.period_start BETWEEN '{}' AND '{}'
                      GROUP BY DA.dw_key, DA.name
                      ORDER BY DA.dw_key ASC""",
    "gender_profile": """SELECT DG.name AS gender, SUM(UP.total_user) AS frequency
                         FROM `dashboard_dev.sm_user_profile` UP
                         JOIN `dashboard_dev.d_gender` DG ON UP.gender_key = DG.dw_key
                         WHERE UP.account_key = {} AND UP.subject_key = {} AND UP.period_start BETWEEN '{}' AND '{}'
                         GROUP BY DG.name""",
    "latest_tweet": """SELECT SR.user_name AS account, SR.content AS tweet, DS.name AS sentiment, SR.post_created_at AS date
                       FROM `dashboard_dev.sm_recap` SR
                       JOIN `dashboard_dev.d_sentiment_status` DS ON SR.sentiment_status_key = DS.dw_key
                       WHERE SR.account_key = {} AND SR.subject_key = {} AND SR.period_start BETWEEN '{}' AND '{}'
                       GROUP BY SR.user_name, SR.content, DS.name, SR.post_created_at
                       ORDER BY SR.post_created_at DESC""",
    "province_location": """WITH d_province AS (SELECT DISTINCT(province_key), province_name
                                                FROM `dashboard_dev.d_location`)
                            SELECT DP.province_name AS name, SUM(SL.total_post) AS frequency, (SELECT SUM(total_post) FROM `dashboard_dev.sm_location_summary`) AS total_frequency
                            FROM `dashboard_dev.sm_location_summary` SL
                            JOIN d_province DP ON DP.province_key = SL.province_key
                            WHERE SL.account_key = {} AND SL.subject_key = {} AND SL.period_start BETWEEN '{}' AND '{}'
                            GROUP BY DP.province_name
                            ORDER BY frequency DESC""",
    "top_cities": """SELECT DL.name AS name, SUM(SL.total_post) AS frequency, (SELECT SUM(total_post) FROM `dashboard_dev.sm_location_summary`) AS total_frequency
                        FROM `dashboard_dev.sm_location_summary` SL
                        JOIN `dashboard_dev.d_location` DL ON SL.city_key = DL.dw_key
                        WHERE SL.account_key = {} AND SL.subject_key = {} AND SL.period_start BETWEEN '{}' AND '{}'
                        GROUP BY DL.name
                        ORDER BY frequency DESC""",
    "kol": """SELECT DS.name AS sentiment_type, ANY_VALUE(IU.retweeted_user) AS account, ANY_VALUE(IU.total_retweet) AS frequency, ANY_VALUE(IU.user_rank)
              FROM `dashboard_dev.sm_influencing_user_weekly` IU
              JOIN `dashboard_dev.d_sentiment_status` DS ON IU.sentiment_status_key = DS.dw_key
              WHERE IU.account_key = {} AND IU.subject_key = {} AND DS.name <> 'Neutral' AND IU.period_start BETWEEN '{}' AND '{}'
              GROUP BY DS.name, IU.user_rank
              ORDER BY DS.name DESC, IU.user_rank ASC""",
    "active_user": """SELECT DS.name AS sentiment_type, ANY_VALUE(AU.active_user) AS account, ANY_VALUE(AU.total_tweet) AS frequency, ANY_VALUE(AU.user_rank)
                      FROM `dashboard_dev.sm_active_user_weekly` AU
                      JOIN `dashboard_dev.d_sentiment_status` DS ON AU.sentiment_status_key = DS.dw_key
                      WHERE AU.account_key = {} AND AU.subject_key = {} AND DS.name <> 'Neutral' AND AU.period_start BETWEEN '{}' AND '{}'
                      GROUP BY DS.name, AU.user_rank
                      ORDER BY DS.name DESC, AU.user_rank ASC""",
    "n_gram_sm": """SELECT ST.topic AS keyword, ANY_VALUE(ST.post_example) AS sample_tweet, SUM(ST.total_post) AS frequency
                 FROM `dashboard_dev.sm_sentiment_topic` ST
                 WHERE ST.account_key = {} AND ST.subject_key = {} AND ST.period_start BETWEEN '{}' AND '{}'
                 GROUP BY ST.topic""",
    "total_news": """SELECT COUNT(*) AS total_news
                     FROM `dashboard_dev.om_recap` OE
                     WHERE OE.account_key = {} AND OE.subject_key = {} AND OE.period_start BETWEEN '{}' AND '{}'""",
    "sentiment_om": """SELECT SUM(total_sentiment_positive) AS positive, SUM(total_sentiment_negative) AS negative, SUM(total_sentiment_neutral) AS neutral
                       FROM `dashboard_dev.om_sentiment_summary` OM
                       WHERE OM.account_key = {} AND OM.subject_key = {} AND OM.period_start BETWEEN '{}' AND '{}'
                       GROUP BY OM.account_key, OM.subject_key""",
    "news_trend": """SELECT OM.period_start AS date, SUM(total_sentiment_positive + total_sentiment_negative + total_sentiment_neutral) AS daily_trend
                     FROM `dashboard_dev.om_sentiment_summary` OM
                     WHERE OM.account_key = {} AND OM.subject_key = {} AND OM.period_start BETWEEN '{}' AND '{}'
                     GROUP BY OM.period_start
                     ORDER BY OM.period_start""",
    "latest_news": """SELECT OE.news_published_at AS date, DS.name AS sentiment, DD.name AS media, OE.title AS title, OE.link AS link
                      FROM `dashboard_dev.om_recap` OE
                      JOIN `dashboard_dev.d_sentiment_status` DS ON OE.sentiment_status_key = DS.dw_key
                      JOIN `dashboard_dev.d_source` DD ON OE.source_key = DD.dw_key
                      WHERE OE.account_key = {} AND OE.subject_key = {} AND OE.period_start BETWEEN '{}' AND '{}'
                      ORDER BY OE.news_published_at DESC""",
    "n_gram_om": """SELECT OT.topic AS keyword, ANY_VALUE(OT.news_example) AS sample_news, SUM(OT.total_news) AS frequency
                    FROM `dashboard_dev.om_sentiment_topic` OT
                    WHERE OT.account_key = {} AND OT.subject_key = {} AND OT.period_start BETWEEN '{}' AND '{}'
                    GROUP BY OT.topic""",
    "media_coverage": """SELECT DD.name AS media, COUNT(*) AS frequency
                         FROM `dashboard_dev.om_recap` OE
                         JOIN `dashboard_dev.d_source` DD ON OE.source_key = DD.dw_key
                         WHERE OE.account_key = {} AND OE.subject_key = {} AND OE.period_start BETWEEN '{}' AND '{}'
                         GROUP BY DD.name""",
}

def retrieve_query(query_type, user_id, topic_id, start_date, end_date):    
    return query_dict[query_type].format(user_id, topic_id, start_date, end_date)