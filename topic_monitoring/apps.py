from django.apps import AppConfig


class TopicMonitoringConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'topic_monitoring'
