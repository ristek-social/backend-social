# Social+ - Backend

## Overview

Social+ is a website that provides social and online media analytics using Web Crawler.

## Technology Stack

- Django 3.2.6
- Django Rest Framework 3.12.4
- PostgreSQL
- Heroku

## Installation

1. Clone the repository
2. ```python -m venv env``` (or ```python3 -m venv env```) to create a virtual environment
3. ```source env/bin/activate``` to activate the virtual environment
4. ```pip install -r requirements.txt``` to install required dependencies
    - Make sure that you have PostgreSQL installed in your device, if not this process will caused error
    - Windows: follow this [tutorial](https://www.postgresqltutorial.com/install-postgresql/)
    - Mac: Use homebrew, ```brew install postgresql```
5. ```source .env``` to use the needed environment variable
6. ```python manage.py makemigrations``` (or ```python3 manage.py makemigrations```) to make migrations
7. ```python manage.py migrate``` (or ```python3 manage.py migrate```) to apply all migrations
8. ```python manage.py runserver``` (or ```python3 manage.py runserver```) to start the server
9. It's recommended to use/load the dummy data, run ```python manage.py loaddata datadump.json``` (or ```python3 manage.py loaddata datadump.json```)

## Changing Database

This [url](https://stackoverflow.com/questions/50322966/changing-django-development-database-from-the-default-sqlite-to-postgresql) provides the tutorial to change the database to PostgreSQL
1. Configure a new database
2. The database configuration is located in ```social_backend/settings.py```, make changes in the ```DATABASES``` variable
3. In local, backup the data first by running the command ```python manage.py dumpdata > datadump.json``` (or ```python3 manage.py dumpdata > datadump.json```)
4. After changing the settings, you can load the data back by using the command ```python manage.py loaddata datadump.json``` (or ```python3 manage.py loaddata datadump.json```)

## API

All API uses ```application/json``` as its request header

### Admin Account
```
email: socialplusadmn@gmail.com
password: s0cialADMIN_
```

#### Admin Feature
1. **Complete Upgrade Membership**
2. **Change Payment Status**
3. **Register New Payment Type**

### Dummy Account
```
email: test_account@gmail.com
password: thisismypasswrod
```

### User Authentication

1. **User Registration**

    API: ```/auth/register/```

    Method: POST

    Request Body:
    ```
    {
        "fullname": "string",
        "username": "string",
        "email": "user@example.com",
        "occupation": "string",
        "password": "string"
    }
    ```
    Constraints:
    - Username and email must be unique
    - Password length ≥ 8
    - Use the code of occupation instead its name, current occupation options
        - CEO: CEO
        - CF: Co-Founder
        - BD: Business Development
        - SE: Software Engineer
        - QA: Quality Assurance Engineer
        - PM: Product Manager
        - GD: Graphic Designer
        - DS: Data Science
        - DO: DevOps
        - DM: Digital Marketing
        - S: Student/University Student
        - O: Other

    Sample Response:
    ```
    {
        "success": true,
        "data": {
            "fullname": "Test User Register",
            "username": "test-user-register",
            "email": "user-test@example.com",
            "occupation": "O"
        }
    }
    ```


2. **Email Verification**

    After user successfully registered, the server will send an email to verify the email.

    API: ```/auth/verify/?token=[generated_token]```

    Method: GET

    Sample Response:
    ```
    {
        "success": true,
        "message": "Email successfully activated"
    }
    ```

3. **User Login**

    User can login once their email is verified.

    API: ```/auth/login/```

    Method: POST

    Request Body:
    ```
    {
        "email": "user@example.com",
        "password": "string"
    }
    ```

    Sample Response:
    ```
    {
        "success": true,
        "data": {
            "username": "test-user-register",
            "email": "user-test@example.com",
            "membership": {
                "membership": "Free",
                "number_of_topics": 1
            },
            "tokens": {
                "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTYzMDQyMzEzMiwianRpIjoiODhhMzA3ZGJmYmU1NDdmODlmYjI3NGRiNDVlYmFmY2YiLCJ1c2VyX2lkIjoxOH0.erqknGV8xAKhJVb8eRFyQV7Ie4TR_PS219bEtKsElE8",
                "access": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjMwMzM3MzMyLCJqdGkiOiIxMWJjY2M0ZTJmNTY0MDcwODM1NWRkNjljNDJiNWY0YSIsInVzZXJfaWQiOjE4fQ.akML4UO3AvnGNflRXoNySKUP9sY67Fwbqt7P0p89emo"
            }
        }
    }
    ```
    - ```number_of_topics``` indicates the number of topics left that user can add
    - ```access``` token is used for user authorization
    - ```refresh``` token is used to request new access token once it expires

4. **Google Auth**

    API: ```/social-auth/google/```

    Method: POST

    Procedures When Using Swagger:

    1. Run the server, and access the Google login button via ```http://localhost:8000/social-auth/index/```
    2. Click the button to register and sign in using your Google account
    3. Once successful, open your browser console then copy the ID Token that will be needed in Swagger

    Request Body:
    ```
    {
        "auth_token": "string"
    }
    ```
    - To retrieve ```auth_token``` has been explained above

    Sample Response:
    ```
    {
        "username": "testaccount",
        "email": "test-account@gmail.com",
        "membership": {
            "membership": "Free",
            "number_of_topics": 1
        },
        "tokens": {
            "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTYzMDQyNTU4MiwianRpIjoiMWViZmE2ZjdkNDliNDEwOThmYTJjYTAyODA2ODE0NGIiLCJ1c2VyX2lkIjoxMX0.e_poZrIl0E1PcHORUvv5jdx5jijqpWb3kdVEDQPxpho",
            "access": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjMwMzM5NzgyLCJqdGkiOiI1NGZiY2ZlNzc5YTc0YjY2ODIzNzRhYzJlYzYxMjA4OSIsInVzZXJfaWQiOjExfQ.6Z22yEHiFN4iOJCQmel2HOhvroxbuEjqHfP4IDm4NHU"
        }
    }
    ```
    - ```number_of_topics``` indicates the number of topics left that user can add
    - ```access``` token is used for user authorization
    - ```refresh``` token is used to request new access token once it expires

5. **User Authorization via Swagger**

    When using Swagger, once user successfully authenticated or logged in, user must be authorized. To do that, click the ```Authorize``` button on the top right of the page and fill in the value with
    ```
    Bearer [access_token]
    ```
    where ```access``` token can be retrieved in **User Login** API response.

6. **Generate New Access Token**

    When the ```access``` token is expired, user must a new one to keep them authorize.

    API: ```/auth/token/refresh/```

    Method: POST

    Request Body:
    ```
    {
        "refresh": "string"
    }
    ```
    - ```refresh``` token can be retrieved when user successfully logged in

    Sample Response:
    ```
    {
        "access": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjMwMzM4MTE5LCJqdGkiOiIxMjhhYjgzYmYzZTc0MDA5YTlhNjBlNjc0YTRlMjBlMiIsInVzZXJfaWQiOjE4fQ.f3iRddycYWBDrfb5EqEkEEWChEVW4fBoGCWvhULLj_w"
    }
    ```

7. **Request Reset Password**

    API: ```/auth/request-reset-password/```

    Method: POST

    Request Body:
    ```
    {
       "email": "user@example.com"
    }
    ```

    Sample Response
    ```
    {
        "success": true,
        "message": "Check your email, we have sent you a link to reset your password"
    }
    ```
    - The system will send user email containing the ````uidb64```` and ```token```

8. **Confirm Reset Password**

    Once the user received email containing ```uidb64``` and ```token```, user can set a new password for their account.

    API: ```/auth/reset-password-complete/```

    Method: PATCH
    
    Request Body:
    ```
    {
        "password": "string",
        "uidb64": "string",
        "token": "string"
    }
    ```

    Sample Response:
    ```
    {
        "success": true,
        "message": "Reset Password Success"
    }
    ```

9. **Retrieve User Profile**

    API: ```/auth/profile/```

    Method: GET

    Constrant: user is authenticated and authorized

    Sample Response:
    ```
    {
        "success": true,
        "data": {
            "user": {
                "fullname": "Test User Register",
                "username": "test-user-register",
                "email": "user-test@example.com",
                "occupation": "Other",
                "auth_provider": "email",
                "membership": "Free",
                "number_of_topics": 1
            },
            "payment": {}
        }
    }
    ```
    - ```payment``` indicates whether the user have requested to upgrade their membership or not

10. **User Logout**

    API: ```/auth/logout/```

    Method: POST

    Request Body:
    ```
    {
        "refresh": "string"
    }
    ```
    - ```refresh``` token can be retrieved when user successfully logged in

    Once successful, there won't be any responses (```204 No Content``` status code)


### User Membership

1. **Get Membership Type**

    API: ```/auth/retrieve-membership-types/```

    Method: GET

    Sample Response:
    ```
    {
        "success": true,
        "data": {
            "membership_type_list": [
                {
                    "id": 1,
                    "type": "Free",
                    "price": 0,
                    "number_of_topics": 1
                },
                {
                    "id": 2,
                    "type": "Premium",
                    "price": 1000000,
                    "number_of_topics": 10
                }
            ]
        }
    }
    ```
    Current membership type options:
    - 1: Free
    - 2: Premium

2. **Request Upgrade Membership**

    API: ```/auth/request-upgrade-membership/```

    Method: POST

    Request Body:
    ```
    {
        "new_membership": 0,
        "payment_type": "string"
    }
    ```

    Constrants:
    - User is authenticated and authorized
    - Use the membership id for the request, current options for membership:
        - 1: Free
        - 2: Premium
    - Use the code of payment type for the request, current options for payment type:
        - BTR: Bank Transfer
    
    Sample Response:
    ```
    {
        "success": true,
        "data": {
            "id": 21,
            "username": "test-user-register",
            "email": "user-test@example.com",
            "old_membership": "Free",
            "new_membership": "Premium",
            "amount": 1000000,
            "payment_type": "Bank Transfer",
            "payment_status": "Pending",
            "created_at": "2021-08-31T00:05:32.254752+07:00",
            "updated_at": "2021-08-31T00:05:32.254789+07:00"
        }
    }
    ```
    - Initial ```payment_status``` will always be ```Pending```

3. **Complete Upgrade Membership**

    When user have paid the membership fee, admin can complete the request.

    API: ```/auth/complete-upgrade-membership/```

    Method: POST

    Constraint:
    - User is authenticated and authorized
    - User is superuser or admin

    Request Body:
    ```
    {
       "payment_id": 0
    }
    ```
    - ```payment_id``` can be retrieved in **Request Upgrade Membership** API

    Sample Response:
    ```
    {
        "success": true,
        "data": {
            "username": "test-user-register",
            "email": "user-test@example.com",
            "old_membership": "Free",
            "new_membership": "Premium",
            "amount": 1000000,
            "payment_type": "Bank Transfer",
            "payment_status": "Complete",
            "created_at": "2021-08-31T00:05:32.254752+07:00",
            "updated_at": "2021-08-31T00:17:32.785970+07:00"
        }
    }
    ```
    - The status become ```Complete```
    - Membership of the user has been changed


### Payment

1. **Get Payment Type**

    API: ```​/payment​/retrieve-payment-type​/```

    Method: GET

    Sample Response:
    ```
    {
        "success": true,
        "message": {
            "payment_type": [
                {
                    "code": "BTR",
                    "name": "Bank Transfer"
                }
            ]
        }
    }
    ```
    Current payment type options:
    - BTR: Bank Transfer

2. **Get Payment Status**

    API: ```​/payment​/retrieve-payment-status/```

    Method: GET

    Sample Response:
    ```
    {
        "success": true,
        "message": {
            "payment_status": [
                {
                    "code": "1",
                    "name": "Pending"
                },
                {
                    "code": "2",
                    "name": "Received"
                },
                {
                    "code": "3",
                    "name": "Complete"
                },
                {
                    "code": "4",
                    "name": "Cancelled"
                }
            ]
        }
    }
    ```
    Current payment status options:
    - 1: Pending
    - 2: Received
    - 3: Complete
    - 4: Cancelled

3. **Change Payment Status**

    When user have paid the membership fee or cancel, admin can change the status of the request.

    API: ```/payment/change-payment-status/```

    Method: POST

    Constraint:
    - User is authenticated and authorized
    - User is superuser or admin

    Request Body:
    ```
    {
       "payment_id": 0,
       "payment_status": "string"
    }
    ```
    - ```payment_id``` can be retrieved in **Request Upgrade Membership** API
    - Use the payment status name not the code, current payment status options:
        - 1: Pending
        - 2: Received
        - 3: Complete
        - 4: Cancelled

    Sample Response:
    ```
    {
        "success": true,
        "data": {
            "username": "test-user-register",
            "email": "user-test@example.com",
            "old_membership": "Free",
            "new_membership": "Premium",
            "amount": 1000000,
            "payment_type": "Bank Transfer",
            "payment_status": "Received",
            "created_at": "2021-08-31T00:05:32.254752+07:00",
            "updated_at": "2021-08-31T00:17:32.785970+07:00"
        }
    }
    ```
    - The status become ```Received```

4. **Register New Payment Type**

    Admin can register new payment type.

    API: ```/payment/register-payment-type/```

    Method: POST

    Constraint:
    - User is authenticated and authorized
    - User is superuser or admin

    Request Body:
    ```
    {
        "code": "string",
        "name": "string"
    }
    ```

    Sample Response:
    ```
    {
        "success": true,
        "data": {
            "code": "CC",
            "name": "Credit Card"
        }
    }
    ```


### Topic and Dashboard

1. **Input New Topic**

    User can input new topic when the user still have a quota.

    API: ```/topic/input/```

    Method: POST

    Request Body:
    ```
    {
        "topic": "string",
        "keyword": "string"
    }
    ```
    - ```keyword``` can be more than one seperated with comma (e.g. keyword A, keyword B, keyword C)

    Sample Response:
    ```
    {
        "success": true,
        "data": {
            "date": "2021-8-30",
            "keyword": [
                "Keyword A",
                "Keyword B",
                "Keyword C"
            ],
            "membership": "Free",
            "topic": "Topic",
            "topic_id": 66,
            "user_id": 18,
            "username": "test-user-register"
        }
    }
    ```

2. **Get List of Topics**

    API: ```/topic/input/```

    Method: GET

    Constrant: user is authenticated and authorized

    Sample Response:
    ```
    {
        "success": true,
        "data": {
            "number_of_topics": 0,
            "topic_list": [
            {
                "id": 66,
                "topic": "Topic",
                "keyword": [
                    "Keyword A",
                    "Keyword B",
                    "Keyword C"
                ],
                "created_at": "2021-08-30T18:16:03.962462Z"
            }
            ]
        }
    }
    ```

3. **Retrieve Dashboard Summary from Client**

    API: ```/topic/dashboard-summary/```

    Method: GET

    Constraint: user is authenticated and authorized

    Request Body:
    ```
    {
        "topic_id": 0,
        "dashboard_type": "string",
        "start_date": "2021-08-30",
        "end_date": "2021-08-30"
    }
    ```
    - ```topic_id``` can be retrieved from **Get List of Topics** API
    - There are currently two ```dashboard_type```:
        - Social Media
        - Online Media
    
    Sample Response:
    ```
    {
        "user": "surili",
        "topic_id": 29,
        "title": "17 Agustus",
        "keyword": [
            "17",
            "Agustus",
            "17 Agustusan",
            "2021",
            "Kemerdekaan"
        ],
        "start_date": "2021-08-21",
        "end_date": "2021-08-28",
        "impression": 36,
        "total_reach": 36,
        "sentiment": {
            "positive": 10,
            "positive_percentage": "27.78",
            "negative": 11,
            "negative_percentage": "30.56",
            "neutral": 15,
            "neutral_percentage": "41.67"
        },
        "daily_exposure": [
            {
                "date": "2021-08-21",
                "exposure": 6
            }
        ],
        "latest_tweets": [
            {
                "account": "socialplusadmn24",
                "tweet": "dah seminggu aja dari 17an",
                "sentiment": "Neutral",
                "date": "2021-08-24T00:00:00Z"
            }
        ],
        "age_profile": [
            {
                "range": "<17",
                "frequency": 6
            }
        ],
        "gender_profile": {
            "Female": 17,
            "Male": 19
        },
        "location": {
            "provinces": [
                {
                    "name": "JK",
                    "frequency": 11
                }
            ],
            "top_provinces": [
                {
                    "name": "JK",
                    "frequency": 11
                }
            ],
            "top_cities": [
                {
                    "name": "Jakarta",
                    "frequency": 11
                }
            ]
        },
        "key_opinion_leader": {
            "positive": [
                {
                    "account": "socialplusadmn1",
                    "frequency": 100
                }
            ],
            "negative": [
                {
                    "account": "socialplusadmn11",
                    "frequency": 50
                }
            ]
        },
        "most_active_users": {
            "positive": [
                {
                    "account": "socialplusadmn4",
                    "frequency": 87
                }
            ],
            "negative": [
                {
                    "account": "socialplusadmn20",
                    "frequency": 19
                }
            ]
        },
        "n_gram": [
            {
                "keyword": "celebrate",
                "frequency": 1,
                "sample_tweet": "alhamdulillah ya kmrn org2 masih celebrate 17an dengan meriah"
            }
        ]
    }
    ```

## Developers
- **Frontend**: Nethania Sonya
- **Backend**: Surya Nirvana

#### Last updated at: 06 September 2021
