from django.urls import path
from .views import GoogleSocialAuthView, index

urlpatterns = [
    path('google/', GoogleSocialAuthView.as_view()),
    path('index/', index, name='index'),
]