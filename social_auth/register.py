from django.contrib.auth import authenticate
from rest_framework.exceptions import AuthenticationFailed
from user_auth.models import Occupation, Subscription, User
import os, random

def register_social_user(provider, user_id, email, fullname):
    filtered_user_by_email = User.objects.filter(email=email)

    if filtered_user_by_email.exists():
        if provider == filtered_user_by_email[0].auth_provider:
            registered_user = authenticate(email=email, password=os.environ.get('SOCIAL_SECRET'))
            subscription = Subscription.objects.get(username=registered_user)
            return {
                'username': registered_user.username,
                'email': registered_user.email,
                'membership': subscription.memberships(),
                'tokens': registered_user.tokens()
            }
        else:
            raise AuthenticationFailed(
                detail='Please continue your login using ' + filtered_user_by_email[0].auth_provider
            )
    else:
        occupation = Occupation.objects.get(code='O')
        user = {
            'username': generate_username(fullname),
            'fullname': fullname,
            'email': email,
            'password': os.environ.get('SOCIAL_SECRET'),
            'occupation': occupation
        }
        user = User.objects.create_user(**user)
        user.is_verified = True
        user.auth_provider = provider
        user.save()

        subscription = Subscription.objects.get(username=user)

        new_user = authenticate(email=email, password=os.environ.get('SOCIAL_SECRET'))
        return {
            'username': new_user.username,
            'email': new_user.email,
            'membership': subscription.memberships(),
            'tokens': new_user.tokens()
        }

def generate_username(fullname):
    username = "".join(fullname.split(' ')).lower()
    if not User.objects.filter(username=username).exists():
        return username
    else:
        random_username = username + str(random.randint(0, 1000))
        return generate_username(random_username)