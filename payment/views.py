from rest_framework import (
    generics,
    status,
    permissions
)
from rest_framework.response import Response

from .models import Payment, PaymentType, PaymentStatus
from .renderers import PaymentRenderer
from .serializers import RegisterNewPaymentTypeSerializer, ChangePaymentStatusSerializer

# Create your views here.
class RegisterNewPaymentTypeAPIView(generics.GenericAPIView):
    serializer_class = RegisterNewPaymentTypeSerializer
    permission_classes =  (permissions.IsAuthenticated, permissions.IsAdminUser,)
    renderer_classes = (PaymentRenderer,)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        data = serializer.data

        return Response({
            'success': True,
            'data': data
        }, status=status.HTTP_201_CREATED)

class RetrievePaymentTypeAPIView(generics.GenericAPIView):
    renderer_classes = (PaymentRenderer,)

    def get(self, request):
        payment_types = PaymentType.objects.all()
        payment_type_list = []

        for payment_type in payment_types:
            dict = {}
            dict["code"] = payment_type.code
            dict["name"] = payment_type.name

            payment_type_list.append(dict)
        
        return Response({
            "success": True,
            "message": {
                "payment_type": payment_type_list,
            }
        }, status=status.HTTP_200_OK)

class ChangePaymentStatusAPIView(generics.GenericAPIView):
    serializer_class = ChangePaymentStatusSerializer
    permission_classes =  (permissions.IsAuthenticated, permissions.IsAdminUser,)
    renderer_classes = (PaymentRenderer,)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        payment = Payment.objects.get(id=request.data['payment_id'])
        payment_status = PaymentStatus.objects.get(name=request.data['payment_status'])
        old_payment_status_id = payment.payment_status.code
        new_payment_status_id = payment_status.code
        if old_payment_status_id > new_payment_status_id or int(old_payment_status_id) == 3:
            return Response({
            'success': False,
            'message': 'Can\'t change status to lower level status'
        }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()

        data = serializer.data

        return Response({
            'success': True,
            'data': data
        }, status=status.HTTP_200_OK)


class RetrievePaymentStatusAPIView(generics.GenericAPIView):
    renderer_classes = (PaymentRenderer,)
    
    def get(self, request):
        payment_statuses = PaymentStatus.objects.all()
        payment_status_list = []

        for payment_status in payment_statuses:
            dict = {}
            dict["code"] = payment_status.code
            dict["name"] = payment_status.name

            payment_status_list.append(dict)
        
        return Response({
            "success": True,
            "message": {
                "payment_status": payment_status_list,
            }
        }, status=status.HTTP_200_OK)