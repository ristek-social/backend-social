from django.urls import path
from .views import (
    RegisterNewPaymentTypeAPIView,
    RetrievePaymentTypeAPIView,
    ChangePaymentStatusAPIView,
    RetrievePaymentStatusAPIView
)

urlpatterns = [
    path('register-payment-type/', RegisterNewPaymentTypeAPIView.as_view(), name='register-payment-type'),
    path('retrieve-payment-type/', RetrievePaymentTypeAPIView.as_view(), name='retrieve-payment-type'),
    path('change-payment-status/', ChangePaymentStatusAPIView.as_view(), name='change-payment-status'),
    path('retrieve-payment-status/', RetrievePaymentStatusAPIView.as_view(), name='retrieve-payment-status'),
]