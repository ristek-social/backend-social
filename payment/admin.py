from django.contrib import admin
from .models import PaymentType, PaymentStatus, Payment

# Register your models here.
class PaymentTypeAdmin(admin.ModelAdmin):
    list_display = ('code', 'name')

class PaymentStatusAdmin(admin.ModelAdmin):
    list_display = ('code', 'name')

class PaymentAdmin(admin.ModelAdmin):
    list_display = ('id', 'username', 'email', 'old_membership', 'new_membership', 'amount', 'payment_type', 'payment_status', 'created_at', 'updated_at')

admin.site.register(PaymentType, PaymentTypeAdmin)
admin.site.register(PaymentStatus, PaymentStatusAdmin)
admin.site.register(Payment, PaymentAdmin)