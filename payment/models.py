from re import sub
from django.db import models
from user_auth.models import User, Membership, Subscription

class PaymentType(models.Model):
    code = models.CharField(max_length=255, db_index=True, null=False, primary_key=True)
    name = models.CharField(max_length=255, db_index=True, null=False)

    def __str__(self):
        return self.name

class PaymentStatus(models.Model):
    code = models.CharField(max_length=255, db_index=True, null=False, primary_key=True)
    name = models.CharField(max_length=255, db_index=True, null=False)

    def __str__(self):
        return self.name

class PaymentManager(models.Manager):
    def create_payment(self, user_id, new_membership, payment_type):
        user = User.objects.get(id=user_id)
        print("error user not exist")
        old_membership = Subscription.objects.get(username=user).membership
        print("error old membership not exist")
        new_membership = Membership.objects.get(type=new_membership)
        print("error onew membership not exist")
        payment_type = PaymentType.objects.get(name=payment_type)
        print("error payment type not exist")
        payment_status = PaymentStatus.objects.get(name='Pending')
        print("error pending not exist")

        payment = self.model(
            username = user,
            old_membership = old_membership,
            new_membership = new_membership,
            payment_type = payment_type,
            payment_status = payment_status
        )
        print("before save")
        payment.save()
        print("after save")

        return payment

    def update_payment(self, payment_id):
        payment = Payment.objects.get(id=payment_id)
        payment_status = PaymentStatus.objects.get(name='Complete')

        user = payment.username
        subscription = Subscription.objects.get(username=user)
        new_membership = payment.new_membership
        subscription.membership = new_membership
        subscription.number_of_topics = new_membership.number_of_topics
        subscription.save()

        payment.payment_status = payment_status
        payment.save()

        return payment
    
    def change_status(self, payment_id, payment_status):
        payment = Payment.objects.get(id=payment_id)
        payment_status = PaymentStatus.objects.get(name=payment_status)

        payment.payment_status = payment_status
        payment.save()

        return payment

class Payment(models.Model):
    username = models.ForeignKey(to=User, related_name='user_payment', on_delete=models.CASCADE, null=False)

    def _get_user_email(self):
        return self.username.email

    def _get_membership_price(self):
        return self.new_membership.price
    
    email = property(_get_user_email)
    old_membership = models.ForeignKey(to=Membership, related_name='user_old_membership', on_delete=models.CASCADE, null=False, default='Free')
    new_membership = models.ForeignKey(to=Membership, related_name='user_new_membership', on_delete=models.CASCADE, null=False, default='Free')
    amount = property(_get_membership_price)
    payment_type = models.ForeignKey(to=PaymentType, related_name='payment_type', on_delete=models.CASCADE, null=False, default='-')
    payment_status = models.ForeignKey(to=PaymentStatus, related_name='payment_status', on_delete=models.CASCADE, null=False, default='Pending')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = PaymentManager()

    def __str__(self):
        return "Payment ID: " + str(self.id) + " (" + self.username.username + ")"
