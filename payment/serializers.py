from django.core.exceptions import ObjectDoesNotExist
from rest_framework import serializers
from .models import PaymentStatus, PaymentType, Payment

class RegisterNewPaymentTypeSerializer(serializers.ModelSerializer):
    code = serializers.CharField()
    name = serializers.CharField()

    class Meta:
        model = PaymentType
        fields = ['code', 'name']

class ChangePaymentStatusSerializer(serializers.ModelSerializer):
    payment_id = serializers.IntegerField(write_only=True)
    payment_status = serializers.CharField()

    username = serializers.CharField(read_only=True)
    email = serializers.CharField(read_only=True)
    old_membership = serializers.CharField(read_only=True)
    new_membership = serializers.CharField(read_only=True)
    amount = serializers.IntegerField(read_only=True)
    payment_type = serializers.CharField(read_only=True)
    created_at = serializers.DateTimeField(read_only=True)
    updated_at = serializers.DateTimeField(read_only=True)

    def validate(self, data):
        payment_id = data.get('payment_id', '')
        payment = Payment.objects.get(id=payment_id)

        if not payment:
            raise ObjectDoesNotExist('Payment does not exist')

        return data
    
    def create(self, validated_data):
        return Payment.objects.change_status(**validated_data)

    class Meta:
        model = Payment
        fields = ['payment_id', 'username', 'email', 'old_membership', 'new_membership', 'amount', 'payment_type', 'payment_status', 'created_at', 'updated_at']